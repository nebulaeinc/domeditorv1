# README #

A custom built dom editor/manipulator that acts as a text editor:
For the full javascript have a look in the comsumer app:

The path is as follows: consumer/static/consumer/js/

### Purpose ###

This is a passion project that developed as an offshoot of a previous project

### Outstanding work ###

Although the use of a double gap buffer was initially satisfactory, he project has increased
in complexity. As such I am migrating the underlying data structure to a rope. 

Additionally I am experimenting with the use of swarm.js to help make it a collaborative editor

The underlying engine still nees to be expanded to mimic a conventional text editor

### Current functionality ###
Cursor + enter + highlighting => works

Basic text input works => works

Module insertions => works

Drag => works


* Repo owner or admin
* Other community or team contact