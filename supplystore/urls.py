"""strandly URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf.urls import include, url


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('gateway.urls', namespace="gateway") ),
    url(r'^accounts/', include('accounts.urls', namespace="accounts")),
    url(r'^consumer/', include('consumer.urls', namespace="consumer")),
    url(r'^navibar/', include('navibar.urls', namespace="navibar") ),
    url(r'^twitterapi/', include('twitterapi.urls', namespace="twitterapi") ),
    url(r'^choosem/', include('choosemodule.urls', namespace="choosemodule") ),
    url(r'^toolbox/', include('toolbox.urls', namespace="toolbox") ),
    url(r'^dropbox/', include('dropbox.urls', namespace="dropbox") ),
    url(r'^filemanager/', include('filemanager.urls', namespace="filemanager") ),
]
