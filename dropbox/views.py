from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core import serializers
from django.urls import reverse
from dropbox.models import *
import dropbox
import json
from pymongo import MongoClient

"""
#----------Mongo------------#
mongoClient = MongoClient() #Creates a connection to the MongoDB
db = mongoClient.dropboxApi

#---------DropBox-----------#
access_token ="1YLMMok4-uAAAAAAAAAACWqaIiQvjeKMzrTZP9bzraK6xfgFbYAFPSR77VD08hiX"
user_id = "647444737"
dropboxClient = dropbox.client.DropboxClient(access_token)

label_result_list = []
paths =['/']
files = []


#dropboxLogin('shdeivunglz7yrk','w25vqge9wkfkv0h')
def dropboxLogin(app_key,app_secret):
    flow = dropbox.client.DropboxOAuth2FlowNoRedirect(app_key, app_secret)
    authorize_url = flow.start()
    #you'll want to automatically send the user to the authorization URL
    # and pass in a callback URL
    access_token, user_id = flow.finish('1YLMMok4-uAAAAAAAAAACL0ApfdnM39H16kD4lWbFIw')

    dictionary = {
                    "function": 'dropboxLogin',
                    "script":'',
                    "html": '<div id="dropboxAccountInfo"> Access Token: ' + str(access_token) + ', User ID: ' + user_id + '</div>'}

    result_id = db.dropboxApi.insert(dictionary)



def accountInfo():
    response = dropboxClient.account_info()
    dictionary = {
                    "function": 'accountInfo',
                    "script":'',
                    "html": '<div id="dropboxAccountInfo"> Display name: ' + str(response['display_name']) + ',User id: '+ str(response['uid']) + ',Location: '+ str(response['locale'])+
                    ',Email: '+ str(response['email']) + ',Email verified: ' + str(response['email_verified']) + ',First Name: '+ str(response['name_details']['familiar_name']) +
                    ',Last Name: '+ str(response['name_details']['surname']) +'</div'}
    print dictionary
    #result_id = db.dropboxApi.insert(dictionary)


def uploadFile(path, fileName):
    open_file = open(fileName, 'rb')
    response = dropboxClient.put_file(path + fileName, open_file)
    dictionary = {
                    "function": 'downloadFile',
                    "script":'',
                    "html": '<div id="dropboxUploadFile"> File Uploaded to: ' + str(response['path']) + ',Size: '+ str(response['size']) + '</div'}

    result_id = db.dropboxApi.insert(dictionary)


def searchAllDirectories(fileName):
    index = 0
    response = dropboxClient.search(path='/',query=fileName,file_limit='1000',include_deleted=False)
    if len(response) == 0:
        dictionary_not = {
                        "function": 'searchAllDirectories',
                        "script":'',
                        "html": '<div id="dropboxSearchAllDirectories"> File not found </div'}
        result_id = db.dropboxApi.insert(dictionary_not)
        print dictionary_not
    else:
        dictionary = {
                        "function": 'searchAllDirectories',
                        "script":'',
                        "html": '<div id="dropboxSearchAllDirectories"> File location: '+ str(response[0]['path']) +'</div'}
        result_id = db.dropboxApi.insert(dictionary)

def listFilesInDirectory(directory):
    files_html = ''
    try:
        folder_metadata = dropboxClient.metadata(directory)
        for contents in folder_metadata['contents']:
            if (contents['icon'] == 'folder_user') or (contents['icon'] == 'folder'):
                pass
            else:

                files_html = files_html + "<br> Folder path & file name: " + contents['path'] + ", Size: " + contents['size'] + ", Last modified: " + contents['modified']
    except:
        files_html = files_html + "<br> No files found."

    dictionary = {
                    "function": 'searchAllDirectories',
                    "script":'',
                    "html": '<div id="dropboxListFilesInDirectory">' + files_html + '</div'}
    result_id = db.dropboxApi.insert(dictionary)



def listAllFiles():
    listAllFolders()
    for path in paths:
        folder_metadata = dropboxClient.metadata(path)
        for contents in folder_metadata['contents']:
            if (contents['icon'] == 'folder_user') or (contents['icon'] == 'folder'):
                pass
            else:
                files.insert(len(files), str(contents['path']))

    returnHtml = ''

    for filze in files:
        if returnHtml == '':
            returnHtml = "Files and paths: " + filze
        else:
            returnHtml = returnHtml + ", " + filze

    dictionary = {
                    "function": 'listAllFiles',
                    "script":'',
                    "html": '<div id="dropboxListAllFiles"> '+ str(returnHtml) +'</div'}
    result_id = db.dropboxApi.insert(dictionary)

def fileCopy(from_path, to_path, fileName):
    try:
        response = dropboxClient.file_copy(from_path+fileName, to_path+fileName)
        dictionary = {
                "function": 'fileCopy',
                "script":'',
                "html": '<div id="dropboxFileCopy"> File: ' + fileName + ', Copied from: ' + from_path + ', Copied to: ' + to_path + '</div'}
                #result_id = db.dropboxApi.insert(dictionary)
        print dictionary
    except:
        dictionary = {
                "function": 'fileCopy',
                "script":'',
                "html": '<div id="dropboxFileCopy"> File failed to copy! </div'}
        print dictionary
                #result_id = db.dropboxApi.insert(dictionary)

fileCopy('/', '/testFolder2/', 'working-draft.txt')

def listAllFolders():
        folder_metadata = dropboxClient.metadata('/')
        label_search_list = ['path','icon','size','modified']
        folder_html = ""
        temp_dict={}
        index=0
        for contents in folder_metadata['contents']:
            for label in label_search_list:
                index +=1
                real_temp_dict = {label:contents[label]}

                if label == 'icon':
                    if (contents[label] == 'folder_user') or (contents[label] == 'folder'):
                        paths.insert(len(paths), str(temp_dict['path']))

                temp_dict.update(real_temp_dict)

                if index ==4:
                    if (temp_dict['icon'] == 'folder_user') or (temp_dict['icon'] == 'folder'):
                        folder_html = folder_html + "<br> Folder path & name: " + temp_dict['path'] + ", Size: " + temp_dict['size'] + ", Last modified: " + temp_dict['modified']

                    label_result_list.insert(len(label_result_list), str(temp_dict))
                    temp_dict.clear()
                    real_temp_dict.clear()
                    index = 0

        folder_in_folder_html = ''
        for path in paths:
            if path == '/':
                pass
            else:
                folder_metadata = dropboxClient.metadata(path)
                for contents in folder_metadata['contents']:
                    if (contents['icon'] == 'folder_user') or (contents['icon'] == 'folder'):
                        paths.insert(len(paths), str(contents['path']))
                        folder_in_folder_html = folder_in_folder_html + "<br> Folder path & name: " + contents['path'] + ", Size: " + contents['size'] + ", Last modified: " + contents['modified']


        dictionary = {
                        "function": 'listAllFolders',
                        "script":'',
                        "html": '<div id="dropboxListAllFolders">' + (folder_html + folder_in_folder_html) + '</div>'}
        result_id = db.dropboxApi.insert(dictionary)


def insertContentsOfFile(fileName):
    listAllFolders()
    for filePathContents in files:
        if fileName in filePathContents:
            with dropboxClient.get_file(filePathContents) as f:
                contents = f.read()
                dictionary = {
                                "function": 'insertContentsOfFile',
                                "script":'',
                                "html": '<div id="dropboxInsertContentsOfFile"> '+ str(contents) +'</div'}
                result_id = db.dropboxApi.insert(dictionary)
        else:
            pass


def downloadFile(fileName):
    out = open(fileName, 'wb')

    listAllFolders()
    for filePathContents in files:
        if fileName in filePathContents:

            with dropboxClient.get_file(filePathContents) as f:
                out.write(f.read())
                dictionary = {
                                "function": 'downloadFile',
                                "script":'',
                                "html": '<div id="dropboxDownloadFile"> File Downloaded!</div'}
                result_id = db.dropboxApi.insert(dictionary)
        else:
            dictionary = {
                            "function": 'downloadFile',
                            "script":'',
                            "html": '<div id="dropboxDownloadFile"> Not Found</div'}
            result_id = db.dropboxApi.insert(dictionary)


def listAllSharedFolder():

    listAllFolders()

    shared_folders=[]
    not_shared_folders=[]
    for path in paths:
        try:
            folder_metadata = dropboxClient.metadata(path)
            if len(folder_metadata['shared_folder']) >=1:
                shared_folders.insert(len(shared_folders), str(path))
                print str(path) + " is SHARED"

        except:
            not_shared_folders.insert(len(not_shared_folders),str(path))
            print str(path) + " is NOT SHARED"

    shared_html =''
    not_shared_html =''

    for notShared in not_shared_folders:
        if not_shared_html == '':
            not_shared_html = "NOT SHARED FOLDERS: " + not_shared_html + notShared
        else:
            not_shared_html = not_shared_html + ", " + notShared


    for shared in shared_folders:
        if shared_html == '':
            shared_html = "SHARED FOLDERS: " + shared_html + shared
        else:
            shared_html = shared_html + ", " + shared

    dictionary = {
                    "function": 'listAllSharedFolder',
                    "script":'',
                    "html": '<div id="dropboxListAllSharedFolder">' + shared_html +'<br> ' + not_shared_html +  '</div>'}

    result_id = db.dropboxApi.insert(dictionary)


def checkIfPathIsShared(path):
    path_name = '/' + path
    try:
        folder_metadata = dropboxClient.metadata(path)
        if len(folder_metadata['shared_folder']) >=1:
            dictionary = {
                            "function": 'checkIfPathIsShared',
                            "script":'',
                            "html": '<div id="dropboxCheckIfPathIsShared"> '+ str(path) + ' is a shared folder' +'</div>'}

            result_id = db.dropboxApi.insert(dictionary)
    except:
        dictionary = {
                        "function": 'checkIfPathIsShared',
                        "script":'',
                        "html": '<div id="dropboxCheckIfPathIsShared"> '+ str(path) + ' is a not a shared folder' +'</div>'}
        result_id = db.dropboxApi.insert(dictionary)



#listFilesInDirectory('/testFolder/folderInTestFolder/test11')
#insertContentsOfFile('working-draft.txt')
#downloadFile('working-draft.txt')
#listAllSharedFolder()
#checkIfPathIsShared('corpFiles')
#fileCopy()
#uploadFile('/testFolder/folderInTestFolder/test11/', 'working-draft.txt')
#listAllFiles()
#listAllFolders()
#searchAllDirectories('meftamask.txt')
#downloadFile('working-draft2.txt')
"""
