
var move_gap_forward = function move_gap_forward () {
  //check that postsize is greater than 1 or else array out of bounds exception
  var end = size - (gap_end);
  if(end < (size - 1)){
    // move the charat end to front of gap
    buffer[gap_start] = buffer[end + 1];
    styleBuffer[gap_start] = styleBuffer[end + 1];
    //nullify end position
    buffer[end + 1] = null;
    styleBuffer[end + 1] = null;

    gap_end -= 1;
    gap_start += 1;
  }
  else
  {
    //extend_buffer();
  }
}


var move_gap_back = function move_gap_back () {
  //check that postsize is greater than 1 or else array out of bounds exception
  if(gap_start > 0)
  {
    var end = size - (gap_end);
    // move the char at front to end of gap
    buffer[end] = buffer[gap_start - 1];
    styleBuffer[end] = styleBuffer[gap_start - 1];

    //nulify start position
    buffer[gap_start-1] = null;
    styleBuffer[gap_start-1] = null;

    gap_end += 1;
    gap_start -= 1;
  }
  else
  {

  }
}

 //@current
 //need to take into consideration nulls and stylebuffer values
var insert_char = function insert_char (chr) {
  // alert(gap_start);
  buffer[gap_start] = chr;
  gap_start += 1;
}

var delete_char = function delete_char (){
  buffer[gap_start - 1] = null;
  gap_start--;
}

var insert_multiple_char = function insert_multiple_char (str) {
  str_arr = str.split('');
  for (var i = 0 ; i < str_arr.length; i++)
  {
    insert_char(str_arr[i]);
  }
}

//to be done once selection logic is added
var delete_multiple_char = function delete_multiple_char (start, end){
  //dfs
}


//in memeory will go right up to next physical char i.e
var move_multiple_forward = function move_multiple_forward (num_chr){
  //alert('MMF:: ' + num_chr);
  for(var i = 0; i < num_chr; i++){
    //find physical char
    while(buffer[size - gap_end + 1] == null && gap_end > 1){
      //double nulls ??
      //are inherently handled as they should not be between a -> b
      move_gap_forward();
    }
    //then itt through num_char
    move_gap_forward();//num_chr ++
  }
}

var move_multiple_back = function move_multiple_back (num_chr){
  //alert('MMB:: ' + num_chr);
  for(var i = 0; i < num_chr; i++)
  {
    while(buffer[gap_start - 1] == null && gap_start > 0){
      move_gap_back();
    }
    move_gap_back();
  }
}


// Manage ghost segments to prepare segments for backspace
var manage_ghost_segments = function manage_ghost_segments(){
  var module_or_text = 0; // 1 = text  && 2 = module;
  var search_deletable_content = true;
  while(search_deletable_content){
    if(current_line_segment.prev().index() == -1){
      search_deletable_content = false;
      //end of physical line  == fire backspace-newline methodss
    }else{
        if(current_line_segment.prev().attr('class') != 'line-segment'){
          //you hit a module -- ABORT LOGIC
          //@manipulation-method
          search_deletable_content = false;
          module_or_text = 1;
        }
        else if(current_line_segment.prev().text().length == 0){ //preventative condition for future problems
          current_line_segment.prev().remove();
          //ghost-segment - Delete and continue
        }
        else if(current_line_segment.prev().text().length > 0){
          //make make current_line_segment and current_segment_index = current_line_segment.text().length - 1
          //current_line_segment = current_line_segment.prev();
          //current_segment_index = current_line_segment.prev().text().length -1; //check
          search_deletable_content = false;
        }
    }
  }
  return module_or_text;
}


var back_space_segment_text = function back_space_segment_text(middle_end) {
    //alert() delete backspace teXSt
    if(middle_end == 1){ //middle
      complete_till_prev(); // modify to check for modules?
      delete_char();
      if(current_line_segment.text().length == 1){
        alert("x - 3");
        var del_chr = current_line_segment.text();
        if(current_line_segment.prev().index() >= 0){
          current_line_segment = current_line_segment.prev();
          if(current_line_segment.attr('class') == 'module'){
            current_segment_index = 1;
          }else{
            current_segment_index = current_line_segment.text().length;
          }
          current_line_segment.next().remove();
        }else if(current_line_segment.next().index() != -1){
          current_segment_index = 0;
          current_line_segment  = current_line_segment.next();
          current_line_segment.prev().remove();
        }
        alert("x - 4 ");
        move_cursor_left(del_chr);
      }else if(current_line_segment.text().length > 1){
        var del_chr = current_line_segment.text().charAt(current_line_segment.text().length -1);
        var str = current_line_segment.text().slice(0, current_line_segment.text().length - 1);
        current_line_segment.text(str);
        current_segment_index -= 1;
        move_cursor_left(del_chr);
      }
    }
    else if(middle_end == 2) {
      complete_till_prev(); // modify to check for modules?
      delete_char();
      if(current_line_segment.prev().text().length == 1){ //delete whole segment;
        var del_chr = current_line_segment.prev().text();
        current_line_segment.prev().remove();
        move_cursor_left(del_chr);
      }
      else if(current_line_segment.prev().text().length > 1){ // next segment is greater than two chars
        //alert("opt 2");
        var del_chr = current_line_segment.prev().text().charAt(current_line_segment.prev().length -1);
        var str = current_line_segment.prev().text().slice(0, current_line_segment.text().length - 1);
        current_line_segment.prev().text(str);
        current_segment_index = current_line_segment.prev().text().length;
        move_cursor_left(del_chr);
        current_line_segment = current_line_segment.prev();
      }else{
        alert("you've hit a ghost");
      }
    }
    else {
    }
}

//back_space
var back_space = function back_space () {
  //testbox ---------------------
  alert("x - 1");
  // ----------------------------
  var segment_text = current_line_segment.text().split('');
  //beggining
  var identifier = current_line_segment.attr('class');
  //you are dealing with a module
  if(identifier != 'line-segment'){
     //call module @manipulation-method

  }
  //-------------- YOU ARE DEALING WITH PLAIN TEXT ----------------
  //Beggining of segment
  else if(current_segment_index == 0){
      //Beggining of line
      if(current_line_segment.index() == 0){
        //call move to next-line-segment-cursor. The data has already been depleted
        //just deal with curosr physical movement
        //Begining of para
        if(current_line.index() != 0){//move to new line logic
          var ismodule  = manage_ghost_segments(); // clean up fluff before moving to new line
          if(ismodule == 0){
            //it is a module so you have to delete it and have specific cursor management logic
            //current_line_segment.last();
            //delete previous text /module check for space to move segment ... move segment

          }else{
            delete_module();
            //current_line = current_line.prev();
            //current_line_segment = current_line.
          }
        }else if(current_para.index() > 0){
        //beggining of para not first para
        //1) check for module
        //2) check for fluff[ghost modules]
        //3) merge two paras together
        //4) shift cursor up

        }
      }
      //Beggining of segment
      else{

        var ismodule = manage_ghost_segments();
        if(ismodule == 0){
            back_space_segment_text(2);
        }else{
          //you are deleting a module
          delete_module();
        }
       }
  }
  //you are in the middle||end of a segment
  else if(current_segment_index > 0){
    alert("x - 2");
    back_space_segment_text(1);
  }
}


var delete_module = function delete_module() {
  alert("You are deleting a module");
}

/*
var old_back_space = function old_back_space() {
    //check the targeted segment to manipulate has the module class?
    //if not treat it as plain text
   var segment_text = current_line_segment.text().split('');
    //change the backspace functionality to encompass all of the following additional
    //features i wil implement
    if(current_segment_index == 0){
      if(current_line_segment.index() > 0){
        complete_till_prev();
        delete_char();
        //delete whole segment
        if(current_line_segment.prev().text().length == 1){
          current_line_segment = current_line_segment.prev();
          //choose where to assign the segment and index now
          if(current_line_segment.index() > 0 && current_line_segment.prev().text().length > 0){
            //second condition not necessary due to the fact that one can just have segment index at 0
            alert("1");
            var del_chr = current_line_segment.text().charAt(0);
            current_line_segment = current_line_segment.prev();
            current_line_segment.next().remove();
            current_segment_index = current_line_segment.text().length;
            move_cursor_left(del_chr);
          }else if(current_line_segment.index() == 0){
            alert("2");
            var del_chr = current_line_segment.text().charAt(0);
            current_line_segment.text(''); // NB the chr length must be one
            current_segment_index = 0
            move_cursor_left(del_chr);
          }

        }else if(current_line_segment.prev().text().length > 1){
          alert("3");
          current_line_segment = current_line_segment.prev();
          current_segment_index = current_line_segment.text().length - 1;
          var del_chr = current_line_segment.text().charAt(current_line_segment.text().length -1);
          var str = current_line_segment.text().slice(0, current_line_segment.text().length - 1);
          current_line_segment.text(str);
          move_cursor_left(del_chr);
        }
        //current_segment_index = 0;
      }
    }
    //middle : Delete char when cursor is inbetween two chars in middle of segment
    else if(current_segment_index > 0  && current_segment_index < current_line_segment.text().length){
      alert("4");
      complete_till_prev();
      delete_char();
      split_segment_for_space(segment_text);
      var del_chr = current_line_segment.text().charAt(current_line_segment.text().length -1);
      var str = current_line_segment.text().slice(0, current_line_segment.text().length - 1);
      current_line_segment.text(str);
      move_cursor_left(del_chr);
    }
    //end
    else if(current_segment_index > 0  && current_segment_index == current_line_segment.text().length){
      alert("5");
      complete_till_prev(); // moves the data structure back 1 step ... i.e Gap buffer
      delete_char(); //deletes the char in memory
      current_segment_index = current_line_segment.text().length - 1; //changes the current segment to -1
      var del_chr = current_line_segment.text().charAt(current_line_segment.text().length -1);
      // ^^ the char in physical space to delete
      var str = current_line_segment.text().slice(0, current_line_segment.text().length - 1);
      // copys the text of the segment to be all -1 char and then reassigns the text to sefment
      // in the following line
      current_line_segment.text(str);

      //z-debug
      $("#realtime-debugger").text(" 2 " + del_chr);

      //moves the curor left in physical space by the realtime width of the actual del_char
      move_cursor_left(del_chr);
    }
    else{
      alert('ERROR:: back_space');
    }
    //i think the whole back_space method requires a massive overhaul. It tries
    //to hard to deal with excpetions but at a cost of exponential complexity
}
*/
//RULE:: ANY ACTION THAT OCCURS FROM THE EDGE OF A SEGMENT
//       CARRIES THE ATTRIBUTES OF THE ORIGINATING SEGMENT

var add_space = function add_space(){
  var segment_text = current_line_segment.text().split('');

  //At the end
  if(current_segment_index >= current_line_segment.text().length){
    complete_till_prev();
    insert_seg_at(' ');
    insert_space_buffer();
    move_cursor_right(' ');
  }
  //At the begginging
  else if(current_segment_index == 0 && current_line_segment.text().length >= 1){
    //complete_till_prev();
    complete_till_prev();
    insert_seg_at_before(' ');
    insert_space_buffer();
    move_cursor_right(' ');
  }
  else if(current_segment_index > 0  && current_segment_index < current_line_segment.text().length){
    //of spaces....
    //current_line_segment.charCodeAt(current_segment_index +1) == 160
    if(segment_text[current_segment_index] == ' '){
      complete_till_prev();
      split_segment_for_space(segment_text);
      insert_seg_at(' ');// the end of the first tempSeg i.e tempSeg1
      insert_space_buffer();
      move_cursor_right(' ');
    }//of word
    else if(segment_text[current_segment_index] != ' ' || segment_text[current_segment_index] == null){
      //complete_till_prev();
      split_segment_for_space(segment_text);
      insert_seg_at(' ');// the end of the first tempSeg i.e tempSeg1
      insert_space_buffer();
      move_cursor_right(' ');
    }
    else{
      alert("ERROR:: Add_space");
    }
  }
}

var insert_space_buffer = function insert_space_buffer (){
  // body...
  buffer[gap_start] = ' ';
  gap_start += 1;
}
//Split
//transfer
//insert

//after every style change
//space

var cssVals = null;
var update_css_buffer = function update_css_buffer(){
  // body...
  cssVals = previous_line_segment.css([
    "color", "background-color"
  ]);
}

var copy_style = function copy_style(){
  if(cssVals != null && cssVals != undefined){
      current_line_segment.css(cssVals);
  }
}

//operates from current buffer location
//only if a new style is declared in the middle of a segment or style
var insert_styles =  function insert_styles(){
  // body... will iterat through and insert all styles at currently focused position
  if(cssVals != null){
    for(var i = 0; i <  cssVals.length; i++){
      styleBuffer[gap_start] = cssVals[i];
      insert_char(null);
    }
  }
}

// RULE TO NOTEs: ALL STYLEBUFFERS BEFORE A SEGMENT WILL ALWAYS APPLY TO THAT SEGMENT
var split_segment_for_space = function split_segment_for_space(segmentText) {
  // could use the substring method here but it would not be mirroring the underlying data structures
  var tempSeg1 = $("<span class='line-segment'></span>");
  var tempSeg2 = $("<span class='line-segment'></span>");
  //move_multiple_back(current_segment_index);
  //var segmentText = current_line_segment.text().split('');
  for(var i = 0; i < current_segment_index; i++){
    tempSeg1.append(segmentText[i]);
  }
  for(var i = current_segment_index; i < segmentText.length; i++){
    tempSeg2.append(segmentText[i]);
  }
  var segIndex = current_line_segment.index();
  if(segIndex > current_line.children().length){
    current_line.append(tempSeg1);
    current_line.append(tempSeg2);
  }else if(segIndex < 1){
    current_line.prepend(tempSeg2);
    current_line.prepend(tempSeg1);
  }else{
    current_line.children().eq(segIndex).after(tempSeg1);
    tempSeg2.insertAfter(tempSeg1);
  }
  current_line_segment.remove();
  current_line_segment = tempSeg1;
  current_segment_index = tempSeg1.text().length - 1;
}


var split_segment_for_new_word = function split_segment_for_new_word(segmentText) {
  // could use the substring method here but it would not be mirroring the underlying data structures
  var tempSeg1 = $("<span class='line-segment'></span>");
  var tempSeg2 = $("<span class='line-segment'></span>");
  //move_multiple_back(current_segment_index);
  tempSeg1.append(segmentText[0]);
  for(var i = 1; i < segmentText.length; i++){
    tempSeg2.append(segmentText[i]);
  }
  var segIndex = current_line_segment.index();
  if(segIndex > current_line.children().length){
    current_line.append(tempSeg1);
    current_line.append(tempSeg2);
  }else if(segIndex < 1){
    current_line.prepend(tempSeg2);
    current_line.prepend(tempSeg1);
  }else{
    current_line.children().eq(segIndex).after(tempSeg1);
    tempSeg2.insertAfter(tempSeg1);
  }
  current_line_segment.remove();
  current_line_segment = tempSeg2;
  current_segment_index = tempSeg2.length;
}


//Func:: traverse style_buffer and meta data in underlying data structure
//starting points:: current_line_segment
var complete_till_next = function complete_till_next () {
  while(buffer[size - gap_end + 1] == null && gap_end > 1){
    move_gap_forward();
  }
}

//Func:: travers style_buffer and meta data in underlying data structure
//starting point:: current_line_segment
var complete_till_prev = function complete_till_prev (argument) {
  while(buffer[gap_start - 1] == null && gap_start > 0 ){
      move_gap_back();
  }
}

//param l_index:: index of current line
var insert_seg_at = function insert_seg_at (chr) {
  //1 = space
  if(chr == ' '){
    chr = '&nbsp';
  }
  $("<span class='line-segment'>"+chr+"</span>").insertAfter(current_line_segment);
  current_line_segment = current_line_segment.next();
  current_segment_index = 0;
}

var insert_seg_at_before = function insert_seg_at_before(chr) {
  //1 = space
  if(chr == ' '){
    chr = '&nbsp';
  }

  $("<span class='line-segment'>"+chr+"</span>").insertBefore(current_line_segment);
  current_line_segment = current_line_segment.prev();
  current_line_segment.css({'background-color':'green'});
  current_segment_index = 0;
}

var update_data_cycle = function update_data_cycle() {
  previous_segment_index = current_segment_index;
  previous_line_segment = current_line_segment;
  previous_line = current_line;
  previous_para = current_para;
}

// --------------------End of Data Manipulation -----------------------------------
//-------------------------------------------------------------------------------
//-----------------------------Line Logic --------------------------------------
// [Manager for line logic]
// Background:: Each line is a meta container for line segments that fit within paragraphs
// Function:: Dynamically prepared at runtime/load of document.
// NBd do not confuse with [enter --> new paragraph]
// Calls:: 1) Logic is called at the creation of a new line segment to see if full.
//         2) Logic is called in the addition of a letter to the semgment to see if full.
//
var isLineFull = function isLineFull (singleChar) {
  var line_Width = current_line.width();
  if(singleChar == ' '){
    singleChar = '&nbsp;';
  }
  $("#single-width").html(singleChar);
  var additional_Width = $("#single-width").width();
  line_Width +=  additional_Width;

  if(line_Width > $('#firstPage').width()){
    return true;
  }
  else{
    return false;
  }
}


// ------------------- @Debug -----------------------------------------------------
//on any action update debug so i can see the contents of gap
//buffer array
var debug_itt = 0;

var update_debug_info = function update_debug_info(){
  debug_itt += 1;

  var stringInfo = "";
  stringInfo += "  ***** Debug Info ***** &#13;&#10;";
  stringInfo += "&#13;&#10; Instance# = " + debug_itt;
  stringInfo += "&#13;&#10; Curr Buffer Pos = " + gap_start;
  stringInfo += "&#13;&#10; Curr Buffer End = " + gap_end;
  stringInfo += "&#13;&#10; Size of buffer = " + size;
  stringInfo += "&#13;&#10; Previous segment index" + previous_segment_index;
  stringInfo += "&#13;&#10";

  //calculate buffer insert -- should match cursor
  for (var i = 0; i < buffer.length ; i++){
    if(i == gap_start){
      stringInfo += "[*|*]";
    }

    if(buffer[i] == null){

    }
    else{
      stringInfo += buffer[i];
    }
  };

  $('#buffer-info').html(stringInfo);
}

//TESTING purposes
$('#reset-colors').on('click', function(){
  var all_segments = $('.page').find('.line-segment');
  all_segments.each(function(itt){
    $(this).css({'color':'black'});
    $(this).css({'background-color':'white'});
  });
});
// -------------------------End of Debug -----------------------------------------
//-------------------------------------------------------------------------------

// ---------------- Outstanding
//selection logic
//toolbar for styling
//bullet points
//Module integration
// ------------------------------------------------------------------------------
//-----------------------------INITIALISATION----------------------------------------
var setUpVariables = function setUpVariables () { //manipulation logic initialisation
  previous_line_segment = $(".page div:eq(0) span:eq(0) span:eq(0)"); //0
  previous_line = $(".page div:eq(0)  span:eq(0)"); // 0
  previous_para = $(".page div:eq(0)"); // 0
  previous_segment_index = 0; // 0
  //position the actual gap buffer at the correct area
  //for now hardcoded
  //gap_start = 0;
  //gap_end = 1000 - 72;
  initialise_gap_buffer();

  //keep moving until at beggining of fist para
   while(buffer[size - gap_end + 1] == null && gap_end > 1){
      //double nulls ??
      //are inherently handled as they should not be between a -> b
      move_gap_forward();
  }
}

var initialise_gap_buffer = function initialise_gap_buffer () {
  //function will move back from the end until it reaches the beggining
  //need to determine size of the gap_buffer
  var itt = 0;
  for(var i = (size-1); i >= 0; i--){
    if(buffer[i] == null && styleBuffer[i] == null){
      gap_start = i;
    }
    else{
      break;
    }
  }
  //alert(size - gap_end);
  align_initial_gap_buffer();
}

var align_initial_gap_buffer = function align_initial_gap_buffer () {
  while(gap_start > 0){
    move_gap_back();
  }
  move_gap_back();
}

var destroyUsedData = function destroyUsedData() {
  // destroy objects like segmentpositions that take up too much memory in runtime
}

//=====================================================================================
$(document).on('click', '#reset-debug' , function(e){

});

$(document).on('click', '#show-styleBuffer', function(e){
});

var update_data_debug_2 = function update_data_debug_2 () {
  var data_string = '';
  for(var i = 0; i < size; i++){
    data_string += styleBuffer[i] + '<br/>';
  }
  $('#debug-data').html(data_string);
}

var update_data_debug = function update_data_debug (){
  var data_string = '';
  for(var i = 0; i < size; i++){
    if(buffer[i] == null){
      if(styleBuffer[i] != null){
        data_string += i + '+<br/>';
      }else{
        data_string += i + '*<br/>';
      }
    }
    else{
      data_string += i + ': ' + buffer[i] + '<br/>';
    }
    if(gap_start == i){
      data_string += i + '[*]<br/>';
    }
  }
  $('#debug-data').html(data_string);
}

//----------------------- File Manipulation ---------------------------------------
var asyncOptimisation = function asyncOptimisation (argument) {
  // will go through whole data structure and check that there arnt overlapping styles
  // join multiple spacing etc
}

var asyncSave = function asyncSave (argument) {
  // body...
}
// ====================== End of file Manipulation ===================================
