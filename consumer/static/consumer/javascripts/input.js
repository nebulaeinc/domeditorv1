// ------------------------ INPUT -----------------------------------------------
// key mapping
// enter
// backspace
// insert module
// copy paste cut
// highllight *
function Input(){
    this.input_area      = null;
}

Input.prototype = {
    init : function(){
        this.input_area  = $("#editor");
    },
    getTimeCardData : function(){
          //ajax request
    },
    selectAll : function(){
        this.getTimeCardData();
    },
    message : function(){
       alert("Frame Message");
    },
    //eventhandlers
    enterKey : function (e, caps) {
      //alert(e.which + '  -  ' + e.charCode + ' - ' + e.keyCode);
      var char = null;
      //alert('key  ' + e.keyCode + '  ' + e.which + '  ' + e.charCode + '  ' + caps);
      if(e.which != null){
        char = String.fromCharCode(e.which);
        input_insert_char(char);
      }else{
        char= String.fromCharCode(e.keyCode);
        input_insert_char(char);
      }
      //alert(char);
    },
    enterSpecial : function (e) {
      if(e.which == 32 || e.keyCode == 32){ //change to 32
        e.stopImmediatePropagation();
        e.preventDefault();
        add_space();
        current_segment_index += 1;
        update_data_cycle();
      }
      //detelet button
      else if(e.which == 8 || e.keyCode == 8){
        e.stopImmediatePropagation();
        e.preventDefault();
        input_delete_char();
      }
      else if(e.which == 13 || e.keyCode == 13){
        e.stopImmediatePropagation();
        e.preventDefault();
        alert("enter was pressed");
      }
      else if(e.which == 9 || e.keyCode == 9){
        e.stopImmediatePropagation();
        e.preventDefault();
        alert("tab was pressed");
      }
      else if(e.which == 46 || e.keyCode == 46){
        e.stopImmediatePropagation();
        e.preventDefault();
      }
      //arrows
      else if(e.which == 37 || e.keyCode == 37){
        e.stopImmediatePropagation();
        e.preventDefault();
        alert("left was pressed");
      }
      else if(e.which == 38 || e.keyCode == 38){
        e.stopImmediatePropagation();
        e.preventDefault();
        split_segment_for_space(current_line_segment.text().split(''));
        insert_module_container();
      }
      else if(e.which == 39 || e.keyCode == 39){
        e.stopImmediatePropagation();
        e.preventDefault();
        alert("right was pressed");
      }
      else if(e.which == 40 || e.keyCode == 40){
        e.stopImmediatePropagation();
        e.preventDefault();
        alert("down was pressed");
      }
      // body...
    },
};

//inserts and propagates a module to db
var insert_module_container = function insert_module_container(){
  //method call to ajax code to get module  template
  var temp_module_id = 'm10823';
  styleBuffer[gap_start] = temp_module_id;
  // create the module
  var temp_module = "<div border-color='red'   class='line-segment'><span id='"+temp_module_id+"'  class='module' color='brown'> Hello This is a test! <span id='tester1' color='red'>Check it</span> </span></div>";
  current_line_segment.css({"background-color":"orange"});

  //insert the module
  $(temp_module).insertAfter(current_line_segment);

  current_line_segment.css({"background-color":"blue"});


  alert("stop");
  current_line_segment = current_line_segment.next();
  current_line_segment.css({"border":"1px solid blue"});
  //insert  a blank segment so user can continue writing
  var ghost_seg_mod = $("<span  class='line-segment'></span>");
  $(ghost_seg_mod).insertAfter(current_line_segment);
  current_line_segment = ghost_seg_mod;
  current_line_segment.css({"background-color":"pink"});

  //update cycle
  current_segment_index = 0; //ghost span starts at -1
  update_data_cycle();

  //sort out data structure
  gap_start += 1;

  //--------------- Cursor -------------------------------
  //get width of module
  var mod_width =   $("#"+ temp_module_id).outerWidth();

  //move the cursor
  /*
  var special_case_adjust = 0;
  var e = jQuery.Event( "mousedown", {
  which: 1,
    pageX: $(".cursor").offset().left + mod_width - special_case_adjust,
    pageY: $(".cursor").offset().top
  });
  jump_cursor_in_segment(e);
  */
  var move_rigth_px = mod_width + $(".cursor").offset().left;
  $(".cursor").css({"left": move_rigth_px+"px"});


  //current_line_segment.css({"background-color":"red"});
  //var xx = "<div class='line-segment'>hello hello</div>";
  //current_line.eq(current_segment_index).css({"background-color":"pink"});
  //current_line_segment.append(xx);
}


//loads a module from db
var load_module_container = function load_module_container(){


}

//=========================== End of Input ==========================================
//@ FIX HORRIBLY CONVOLUTED IF ELSE LOGIC
var input_insert_char = function input_insert_char(char) {
  //alert(current_segment_index);
  insert_char(char);
  var a = current_line_segment.text();
  var b = char;

  //space logic/////////////////////////////////////////////////////////
  //beggining
  if(current_segment_index == 0 && a.length > 0 && a.charCodeAt(0) == 160){
    complete_till_prev();
    insert_seg_at_before(char);
    //alert("char inserted1");
  }
  else if(current_segment_index > 0 && current_segment_index < a.length && a.charCodeAt(current_segment_index-1) == 160 ){//middle but not two spaces
    complete_till_prev();
    split_segment_for_space(current_line_segment.text().split(''));
    insert_seg_at(char);
    //alert("char inserted2");
  }else if(a.charCodeAt(current_segment_index-1) == 160){ // prior is a space
    complete_till_prev();
    //alert("char inserted3");
    //split_segment_for_space(current_line_segment.text().split(''));
    insert_seg_at(char);
  }else if(a.charCodeAt(current_segment_index) == 160){
    complete_till_prev();
    split_segment_for_space(current_line_segment.text().split(''));
    insert_seg_at(char);
    //alert("char inserted4");
  }

  ///////////////////////////////////////////////////////////////////////
  else{//insert char at focused postion
    //alert('did this fire');
    current_line_segment.text([a.slice(0, current_segment_index), b, a.slice(current_segment_index)].join(''));
  }
  move_cursor_right(char);
  current_segment_index += 1;
  update_data_cycle();
}


var input_delete_char = function input_delete_char() {
  back_space();
  update_data_cycle();
}

//#ready
//document ready p:2 ..... awaiting merge
$(document).ready(function(){
  var input  = new Input();
  input.init();
  var cursor_is_on = true; // for the flash
  var editor_active = false;
  trumps_border_control_off();

  //Constants for key Logic

  //Event Propogation == bubbling
  //*********************************************************************************
  function trumps_border_control_on(){
    input.input_area.css({'border-color':'#2266ff'});
  }

  function trumps_border_control_off(){
    input.input_area.css({'border-style':'solid'});
    input.input_area.css({'border-color':'#ffffff'});
    input.input_area.css({'border-size':'0.2px'});
  }

  function wake_editor(){
    if(editor_active){
    }else{
      trumps_border_control_on();
      editor_active = true;
    }
  }

  //Might not work if scrollbar is enabled
  function find_cursor_loc(e){
    var x = e.pageX;
    var y = e.pageY;
    var binding_line = null;
    //alert(y);

    //change to split and check according to quarters eights 16ths
    //dependant on line number
    $( ".line" ).each(function( index ) {
      //alert('top: ' + $(this).offset().top + '  bottom: ' + ($(this).offset().top + $(this).height()) );

      if(binding_line == null){
        binding_line = $(this);
      }
      var rect = $(this)[0].getBoundingClientRect();

      //if line bottom is less than y
      if($(this).offset().top + $(this).height() < y){ //adjust with offests
        //if line bottom is greater than previous bottom
        if(rect.bottom > (binding_line.position().top + binding_line.height()) ){
          binding_line = $(this);
        }
      }
      //if line top is less than y but line bottom is greater than y
      if($(this).offset().top < y && ($(this).offset().top + $(this).height())  > y ){
          binding_line = $(this);
          return false;
      }
      if( $(this).offset().top > y && ($(this).offset().top + $(this).height())  > y ){
        return false;
      }
    });

    //Have to map out the above line-segment
    //now move cursor to the end
    //current_line = binding_line;
    current_line = binding_line;
    e.pageY = binding_line.offset().top + (binding_line.height()/2); //adjust to centre of line
    var enclosed_segments =  binding_line.children().toArray(); //just to check that array is not empty
    var line_segment_ref = null;

    binding_line.children('.line-segment').each(function(index){
      if(enclosed_segments.length == 0 || enclosed_segments == null){
      //create a segment for the current line
      }
      else{
        if(line_segment_ref ==null){
          line_segment_ref = $(this);
        }
        if($(this).offset().left < x){
            line_segment_ref = $(this);
            //debug
        }
        else if($(this).offset.left > x){
            return false;
        }
      }
    });

    current_line_segment = line_segment_ref;
    if(line_segment_ref.offset().left < x && (line_segment_ref.offset().left + line_segment_ref.width() ) > x ){
      //do nothing
      //once i know the line segment -> call cursor functions
      current_segment_index = jump_cursor_in_segment(e);
      click_cursor(current_segment_index);
    }else if(x > line_segment_ref.offset().left && x > line_segment_ref.offset().left + line_segment_ref.width() ){
      e.pageX = line_segment_ref.offset().left + line_segment_ref.width() - 0.01;
      //adjust this for all browsers and for font
      current_segment_index = jump_cursor_in_segment(e);
      click_cursor(current_segment_index);
    }
  }

  //page
  $(document).on('click', function(e){
    //alert("document");
    trumps_border_control_off();
    editor_active = false;

    cursor_off();
    cursor_active = false;
  });

  //editor
  $("#editor").on('click', function(e){
    e.stopImmediatePropagation();
    wake_editor();

    cursor_off();
    cursor_active = false;
  });

  $("#editor").on('click', '.page',function(e){
    wake_editor();
    find_cursor_loc(e);
    update_debug_info();
    e.stopImmediatePropagation();

    cursor_on();
    cursor_active = true;
  });

  //module
  $("#editor").on('click', '.module', function(e){
    e.stopImmediatePropagation();
    wake_editor();
    var mod_left_mov = $(this).offset().left + $(this).outerWidth();
    $('.cursor').css({"left": mod_left_mov+ "px"});
    if($(this).next().index() != -1){

    }else{
      current_line_segment = $(this).next();
      current_segment_index = 0;
      update_data_cycle();
    }
  });

  //tabbed spacing
  $("#editor").on('click', '.tabbed', function(e){
    e.stopImmediatePropagation();
    wake_editor();
  });

  //line-segment
  $("#editor").on('click', '.line-segment', function(e){
    e.stopImmediatePropagation();
    wake_editor();
    current_line_segment = $(this);
    current_segment_index = jump_cursor_in_segment(e);

    if(check_edge_cases()){
      click_cursor(current_segment_index);
    }
    cursor_on();
    cursor_active = true;
    //@test
    //copy_style();
    //update_css_buffer();
    //alert(current_line_segment.text().charCodeAt(current_segment_index-1));
    //alert('index: ' + current_segment_ia
    //alert('csi:: ' + current_segment_in   dex);
  });
//mmf ---> *      -----> *
//mmb * <----         * <----

  var check_edge_cases = function check_edge_cases () {
    var edge_flag = true;
    current_line = current_line_segment.parent();
    current_para = current_line.parent();
    //logic to ensure segment is focused on word <--
    //does it need a move_till_prev?
    var index_of_seg = current_line_segment.index();
    //if u are at begginging and there is a space in front of you
    if(current_segment_index == 0 && current_line_segment.text().charCodeAt(0) == 160 && index_of_seg > 0 ){
      current_line_segment = current_line_segment.prev();
      current_segment_index = current_line_segment.text().length;
      click_cursor(current_segment_index);
      complete_till_prev();
      //move_till_prev();
      edge_flag  = false;
      //alert('edge case 1 fired');
    }//you've clicked at the beggining of a new para
     else if(index_of_seg == 0 && current_line.index() != 0 && current_segment_index == 0){
      click_cursor(current_segment_index);
      complete_till_next();
      edge_flag  = false;
      //alert('edge case 2 fired');
    }//you've clicked at the beggining of a new para
    else if(index_of_seg == 0 && current_line.index() == 0 && current_segment_index == 0){
      click_cursor(current_segment_index);
      complete_till_next();
      edge_flag  = false;
      //alert('edge case 3 fired');
    }
    //you've clicked at the begginging of segment at the beggining of the 1st line
    else if(index_of_seg == 0 && current_segment_index == 0 && current_line && current_line.index() == 0){
      //if the only segment in para
      //common in new paras where the will be an inital empty segment
      if(current_line.children().length <= 1){
        //enable flag for the move multiple back function
        click_cursor(current_segment_index);
        edge_flag = false;
      }
    }
    /*else if(current_segment_index < current_line_segment.text().length && current_segment_index > 0){
      //Youre surrounded by spaces ---> Put Yur Hands up
      if(current_line_segment.text().charCodeAt(current_segment_index) == 160 && current_line_segment.text().charCodeAt(current_segment_index - 1) == 160){

      }
    }*/
  return edge_flag;
  }

  var left_margin_align_line = function left_margin_align_line (argument) {
    alert('testing 2');
  }

  var left_margin_align_para = function left_margin_align_para (argument) {
      alert('testing');
  }

//creates a new paragraph with an empty segment initialised
//text is appended to this segment
  var enter_para = function enter_para () {
    // body...
  }

  //for a click not on line segement but still on editor
  //cursor functionality
  //make sure cursor is on (i.e document active before commiting text)
  cursor_flash = function cursor_flash () {
      if(cursor_is_on == true)
      {
        cursor_off();
      }
      else if (cursor_is_on == false)
      {
        cursor_on();
      }
  }

  var cursor_off = function cursor_off (){
    visual_cursor.css({'visibility':'hidden'});
    cursor_is_on = false;
  }
  var cursor_on = function cursor_on (){
    visual_cursor.css({'visibility':'visible'});
    cursor_is_on = true;
  }

  var caps = false;
  //keypress event
  $(document).on('keydown', input.input_area, function(e) {
    if(cursor_active){
      //alert("special case");
      input.enterSpecial(e);
    }
  });

  $(document).on('keypress', input.input_area, function(e){
    if(cursor_active == true && e.charCode != 0){
      if((e.keyCode >= 65 && e.keyCode <= 90) || (e.which >= 65 && e.which <= 90) ){
        //alert("we got in");
        input.enterKey( e, false);
      }
      else if((e.keyCode >= 97 && e.keyCode <= 122) || (e.which >= 97 && e.which <= 122) ){
        //alert("we got in in");

        input.enterKey( e, true);
      }
    }
  })

  function styling_change_detected(){
    //sets current buffer[i] pos to null and updates stylebuffer
  }


  var browser_detection_script = function browser_detection_script () {
  //Author:: Kennebec from stack overlow
  //body...
    var ua= navigator.userAgent, tem,
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
  }
});


//author: rajesh pilai
function isCapslock(e){

    e = (e) ? e : window.event;

    var charCode = false;
    if (e.which) {
        charCode = e.which;
    } else if (e.keyCode) {
        charCode = e.keyCode;
    }
    var shifton = false;
    if (e.shiftKey) {
        shifton = e.shiftKey;
    } else if (e.modifiers) {
        shifton = !!(e.modifiers & 4);
    }
    if (charCode >= 97 && charCode <= 122 && shifton) {
        return true;
    }
    if (charCode >= 65 && charCode <= 90 && !shifton) {
        return true;
    }
    return false;
}
