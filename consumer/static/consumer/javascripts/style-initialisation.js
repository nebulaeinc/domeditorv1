var load_styles = function load_styles () {
  var allSegments = document.getElementsByClassName("line-segment");
  current_line_segment = null;
  var currSegmentIndex = 0; // stores the current segment index
  var startIndexOfSegment = 0;
  var endIndexOfSegment = 0;
  var styleS = null;
  var prefix = null;
  //how to deal with empty segments ??

  //active style variables
  var current_style_colour = null;
  var color_endPos = null; //[] first val holds a color

  var current_style_size = null;
  var size_endPos = null; //[]

  var current_style_font = null;
  var font_endPos = null; //[]

  var current_style_bold = null;
  var bold_endPos = null; //[]

  var current_style_underline = null;
  var underline_endPos = null; //[]

  var current_style_italic = null;
  var italic_endPos = null; //[]

  //as we go through the styling we encounter new styles we push them in  and need to remember
  // 1) the end of the curr one in the buffer
  // 2) the actual value
  // the inherit orderimplies the order they went into the collection

  var colorMap = [];
  // end of initialisation


  //loop through entire buffer an style
  for(var i = 0; i  < buffer.length; i++){
    // -------------- This logic adds segments to specific line ------
    // -------------- good for bullet point styling etc --------------
    // ----------It also remembers current segment -------------------
    startIndexOfSegment = segmentPositions[currSegmentIndex];

    if(currSegmentIndex < segmentPositions.length){ //you still have enough segments
      endIndexOfSegment = segmentPositions[currSegmentIndex + 1]; // where curr segment ends
      if(i >= endIndexOfSegment){  // if current buffer has exceeded end of segment you are in new current segment
        currSegmentIndex++;
        current_line_segment = allSegments[currSegmentIndex];
      }else{
        current_line_segment = allSegments[currSegmentIndex];
      }
    }
    else{ // we've hit the end of the line
      endIndexOfSegment = buffer.length;
      startIndexOfSegment = segmentPositions[segmentPositions.length];
      current_line_segment = allSegments[allSegments.length];
    }

    //segmentPositions stores start position of all segments
    // ---------------End of logic --------------------------------
    //regex
    // /\|/g
    // /\#(.*?)\|/g;

    if(buffer[i] == null && styleBuffer[i] != null){ //weve found a style key
      var styleS = styleBuffer[i];
      var prefix = styleS.substring(0,2);

      // COLOR STYLE
      if(prefix == '/#'){ // its a colour style
        //color_endPos = parseInt(styleS.split(/\|/g)[1]);
        //current_style_colour = /\#(.*)\|/g.exec(styleS)[1];
        //current_style_colour = '#' + current_style_colour;

        var styleProperties = {
          propertyVal: '#' + (/\#(.*)\|/g.exec(styleS)[1]),
          endPos: parseInt(styleS.split(/\|/g)[1])
        };

        colorMap.unshift(styleProperties);
      }
      //SIZE STYLE
      else if(prefix == '/s'){
        //do font size
      }
      //FONT STYLE
      else if(prefix == '/f'){
        //do font type
      }
    }
    //******
    //*red *
    //*blue*
    //******
    if(colorMap.first() != null){
      if(i > colorMap.first().endPos ){ //by this logic the last number is included in the styling
        colorMap.shift(); //keep popping until we reach endpos that works
        if(colorMap.first() != null && colorMap.first() != undefined){ // its empty
          if(i > colorMap.first().endPos ){ //check if iteration needed [often unlikely]
          var foundViableEndPos = false;
          var endPos = colorMap.first();
            while(!foundViableEndPos){ // now keep shifting until endPos is greater than i
              if(i > colorMap.first().endPos){ //by this logic the last number is included in the styling
                colorMap.shift();
                if(colorMap.first() == null || colorMap.first() == undefined){ // its empty
                  foundViableEndPos = true;
                }else{
                  endPos = colorMap.first();
                }
              }else{
                foundViableEndPos = true;
              }
            }
          }else{
            //do nothing
          }
        }else{
          //do nothing
        }
      }


      //we've found a colorStyle to work with. Now add it to the current segment
      if(colorMap.first() != null && colorMap.first() != undefined){ // its empty
        if(i <= colorMap.first().endPos){
          current_line_segment.style.color = colorMap[0].propertyVal;
        }
      }
    }else{
      //console.log("Do nothing: ColorMap is depleted");
    }
  }
}

if (!Array.prototype.last){
  Array.prototype.first = function(){
      return this[0];
  };
};

//method takes in start pos and end pos of buffer and
//colors in all segments inBetween
var colorInSegments = function colorInSegments(startbufferPosition, endBufferPosition){
  //match position to pos in the relevant segment ????
  // numOfSegments
  // segmentPositions [] ---- stores the start pos of each segment

  //
  var allSegments = document.getElementsByClassName("line-segment");
  var currSegment = 0;
  // 67 -- 22 segments
}
