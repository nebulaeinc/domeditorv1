var currently_selected  = null;
var currently_selected_data = null;
var currently_selected_label = null;
var data = null;
var options = null;
var ctx = null;
var myChart = null;
var gl = null;
var current_graph = "bar";
var chartTitle = "My Chart Title";
var targeted_graph_module = null;
var graph_x_axis = "";
var graph_y_axis = "";

//This is the functionality for a bar -- chart
$(document).ready(function(){

    //refocus("myChart");

  $("#table-add-dataset").on('click', function(){
      $("#table-color-input-row").append("<td></td>");
      $("#table-data-input-row").append("<td contenteditable></td>");
      $("#table-label-input-row").append("<td><input class='table-graph-in' type='text' name='lname'></td>");
  });

  $("#table-remove-dataset").on('click', function(){
    if($("#table-color-input-row").children().length > 1){
      $("#table-color-input-row").children().last().remove();
      $("#table-data-input-row").children().last().remove();
      $("#table-label-input-row").children().last().remove();
    }
  });

  $(document).on('click', '#table-color-input-row > td',function(){
    if(currently_selected != null){
      currently_selected.css({"border": "1px solid grey"});
      currently_selected.removeClass("selected-table-color-input");
    }
    currently_selected = $(this);
    currently_selected.addClass("selected-table-color-input");
    currently_selected.css({"border":"2px solid blue"});
  });

  $(document).on('click', '#table-data-input-row > td',function(){
    if(currently_selected_data != null){
      currently_selected_data.css({"border": "1px solid grey"});
      currently_selected_data.removeClass("selected-table-color-input");
    }
    currently_selected_data = $(this);
    currently_selected_data.addClass("selected-table-color-input");
    currently_selected_data.css({"border":"2px solid blue"});
  });

  $(document).on('click', '#table-label-input-row > td',function(){
    if(currently_selected_label != null){
      currently_selected_label.css({"border": "1px solid grey"});
      currently_selected_label.removeClass("selected-table-color-input");
    }
    currently_selected_label = $(this);
    currently_selected_label.addClass("selected-table-color-input");
    currently_selected_label.css({"border":"2px solid blue"});
  });

  function resizeInput() {
      $(this).attr('size', $(this).val().length);
  }

  $('.table-graph-in')
      // event handler
      .keyup(resizeInput)
      // resize on page load
      .each(resizeInput);

}); //end of document.ready

var refocus  = function refocus(chartName) {
  ctx = document.getElementById(chartName);
  gl = new graphLoader();
  gl.loadBar();
}//end of refocus

$(document).on('click', '#graph_detail_commit',function(){
  chartTitle = document.getElementById("charTitle").value;
  graph_x_axis = document.getElementById("ctx-x-axis").value;
  graph_y_axis = document.getElementById("ctx-y-axis").value;

  gl.loadBar();

});

var chekit = function chekit(selectedObj) {
    var s = selectedObj.value;
    var patt = /^#([\da-fA-F]{2})([\da-fA-F]{2})([\da-fA-F]{2})$/;
    var matches = patt.exec(s);
    var rgb = "rgba("+parseInt(matches[1], 16)+","+parseInt(matches[2], 16)+","+parseInt(matches[3], 16)+ ", 0.2" +")";
    var rgbBorder = "rgba("+parseInt(matches[1], 16)+","+parseInt(matches[2], 16)+","+parseInt(matches[3], 16)+ ", 1" +")";

    alert(data.datasets[0].backgroundColor[1]);

    data.datasets[0].backgroundColor[currently_selected.index()-1] = rgb;
    data.datasets[0].borderColor[currently_selected.index()-1] = rgbBorder;

    myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: options
    });

     currently_selected.css({"background-color":selectedObj.value});
  }

  var createGraph = function createGraph() {
    var arrLength = $("#table-label-input-row").children().length -1;
    data.labels = [];
    $("#table-label-input-row").find('input').each (function() {
      data.labels.push($(this).val());
    });

    data.datasets[0].data = [];
    $("#table-data-input-row").find('td').each (function() {
      data.datasets[0].data.push(parseInt($(this).text()));
    });

    $("#table-color-input-row").find('td').each (function() {
      var s = $(this).css('background-color');
      var x = s.split(")");
      var y = x[0].split("(");
      var rgb  = "rgba("+y[1] + ", 0.3)";
      var rgb2 = "rgba("+y[1] + ", 1)";
      data.datasets[0].backgroundColor.push(rgb);
      data.datasets[0].borderColor.push(rgb2);
    });

    $("#table-hover-input-row").find('td').each (function() {
    });

    $("#myChart").remove();
    $(".table-container").append("<canvas id='myChart'></canvas>");

    ctx = document.getElementById("myChart");
    myChart = new Chart(ctx, {
        type: current_graph,
        data: data,
        options: options
    });
    alert(data.datasets[0].backgroundColor[0] + " " + data.datasets[0].backgroundColor[1] + " " + data.datasets[0].backgroundColor[2]);
}

var loadBarControl = function loadBarControl() {
  gl.reload();
  gl.loadBar();
  current_graph = "bar";
  gl.loadBarTools();
}

class graphLoader { //constructor
  constructor(){}
}

//reload targeted canvas
graphLoader.prototype.reload = function() {
  $("#myChart").remove();
  $(".table-container").append("<canvas id='myChart'></canvas>");
  ctx = document.getElementById("myChart");
}


graphLoader.prototype.loadBar = function() {
   data = {
       labels: ["Red", "Blue", "Yellow"],
       datasets: [{
           label: '# of Votes',
           data: [12, 19, 3],
           backgroundColor: [
               'rgba(255, 99, 132, 0.2)',
               'rgba(54, 162, 235, 0.2)',
               'rgba(255, 206, 86, 0.2)'
           ],
           borderColor: [
               'rgba(255,99,132,1)',
               'rgba(54, 162, 235, 1)',
               'rgba(255, 206, 86, 1)'
           ],
           borderWidth: 1
       }]
   };
   options = {
       scales: {
           yAxes: [{
               ticks: {
                   beginAtZero:true
               },
               scaleLabel: {
                 display: true,
                 labelString: graph_y_axis
               }
           }],
           xAxes: [{
               ticks: {
                   beginAtZero:false
               },
               scaleLabel: {
                 display: true,
                 labelString: graph_x_axis
               }
           }]
       },
       title: {
            display: true,
            text: chartTitle
        }
   }
   $(".table-container").css({"height":"220px"});
   myChart = new Chart(ctx, {
       type: 'bar',
       data: data,
       options: options
   });
}//end of function
