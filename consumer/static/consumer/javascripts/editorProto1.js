
var wipe_css = function wipe_css () {
  // body...
  $('.line-segment').each(function(){
    $(this).css({'background-color':'white'});
  });
}

function Input(){
    this.input_area = null;
}

Input.prototype = {
    init : function(){
        this.input_area  = $("#editor");
    },
    getTimeCardData : function(){
          //ajax request
    },
    selectAll : function(){
        this.getTimeCardData();
    },
    message : function(){
       alert("Frame Message");
    },
    //eventhandlers
    enterKey : function (e, caps) {
      //alert(e.which + '  -  ' + e.charCode + ' - ' + e.keyCode);
      var char = null;
      //alert('key  ' + e.keyCode + '  ' + e.which + '  ' + e.charCode + '  ' + caps);
      if(e.which != null){
        char = String.fromCharCode(e.which);
        input_insert_char(char);
      }else{
        char= String.fromCharCode(e.keyCode);
        input_insert_char(char);
      }
      //alert(char);
    },
    enterSpecial : function (e) {
      if (mod_is_targeted != true){
      if(e.which == 32 || e.keyCode == 32){ //change to 32
        e.stopImmediatePropagation();
        e.preventDefault();
        add_space();
        current_segment_index += 1;
        update_data_cycle();
      }
      //detelet button
      else if(e.which == 8 || e.keyCode == 8){
        e.stopImmediatePropagation();
        e.preventDefault();
        input_delete_char();
      }
      else if(e.which == 13 || e.keyCode == 13){ // Enter Button
        e.stopImmediatePropagation();
        e.preventDefault();
        enter_func();
      }
      else if(e.which == 9 || e.keyCode == 9){
        e.stopImmediatePropagation();
        e.preventDefault();
        alert("tab was pressed");
      }
      else if(e.which == 46 || e.keyCode == 46){
        e.stopImmediatePropagation();
        e.preventDefault();
      }
      //arrows
      else if(e.which == 37 || e.keyCode == 37){
        e.stopImmediatePropagation();
        e.preventDefault();
        alert("left was pressed");
      }
      else if(e.which == 38 || e.keyCode == 38){
        e.stopImmediatePropagation();
        e.preventDefault();
        split_segment_for_space(current_line_segment.text().split(''));
        insert_module_container();
      }
      else if(e.which == 39 || e.keyCode == 39){
        e.stopImmediatePropagation();
        e.preventDefault();
        alert("right was pressed");
      }
      else if(e.which == 40 || e.keyCode == 40){
        e.stopImmediatePropagation();
        e.preventDefault();
        alert("down was pressed");
      }
     }
      // body...
    },
};


//inserts and propagates a module to db
var insert_module_container = function insert_module_container(){

  module_options(function(output){
    $("#module-options-list").html(output);
    $("#module-options-list").css({'visibility':'visible'});
  });
}


//loads a module from db
var load_module_container = function load_module_container(){


}

//=========================== End of Input ==========================================
//@ FIX HORRIBLY CONVOLUTED IF ELSE LOGIC
var input_insert_char = function input_insert_char(char) {
  //alert(current_segment_index);
  insert_char(char);
  var a = current_line_segment.text();
  var b = char;

  //space logic/////////////////////////////////////////////////////////
  //beggining
  if(current_segment_index == 0 && a.length > 0 && a.charCodeAt(0) == 160){
    complete_till_prev();
    insert_seg_at_before(char);
    //alert("char inserted1");
  }
  else if(current_segment_index > 0 && current_segment_index < a.length && a.charCodeAt(current_segment_index-1) == 160 ){//middle but not two spaces
    complete_till_prev();
    split_segment_for_space(current_line_segment.text().split(''));
    insert_seg_at(char);
    //alert("char inserted2");
  }else if(a.charCodeAt(current_segment_index-1) == 160){ // prior is a space
    complete_till_prev();
    //alert("char inserted3");
    //split_segment_for_space(current_line_segment.text().split(''));
    insert_seg_at(char);
  }else if(a.charCodeAt(current_segment_index) == 160){
    complete_till_prev();
    split_segment_for_space(current_line_segment.text().split(''));
    insert_seg_at(char);
    //alert("char inserted4");
  }

  ///////////////////////////////////////////////////////////////////////
  else{//insert char at focused postion
    //alert('did this fire');
    current_line_segment.text([a.slice(0, current_segment_index), b, a.slice(current_segment_index)].join(''));
  }
  move_cursor_right(char);
  current_segment_index += 1;
  update_data_cycle();
}


var input_delete_char = function input_delete_char() {
  back_space();
  update_data_cycle();
}


//author: rajesh pilai
function isCapslock(e){

    e = (e) ? e : window.event;

    var charCode = false;
    if (e.which) {
        charCode = e.which;
    } else if (e.keyCode) {
        charCode = e.keyCode;
    }
    var shifton = false;
    if (e.shiftKey) {
        shifton = e.shiftKey;
    } else if (e.modifiers) {
        shifton = !!(e.modifiers & 4);
    }
    if (charCode >= 97 && charCode <= 122 && shifton) {
        return true;
    }
    if (charCode >= 65 && charCode <= 90 && !shifton) {
        return true;
    }
    return false;
}


// ------------------- GAP BUFFER --------------------------------

var move_gap_forward = function move_gap_forward () {
  //check that postsize is greater than 1 or else array out of bounds exception
  var end = size - (gap_end);
  if(end < (size - 1)){
    // move the charat end to front of gap
    buffer[gap_start] = buffer[end + 1];
    styleBuffer[gap_start] = styleBuffer[end + 1];
    //nullify end position
    buffer[end + 1] = null;
    styleBuffer[end + 1] = null;

    gap_end -= 1;
    gap_start += 1;
  }
  else
  {
    //extend_buffer();
  }
}


var move_gap_back = function move_gap_back () {
  //check that postsize is greater than 1 or else array out of bounds exception
  if(gap_start > 0)
  {
    var end = size - (gap_end);
    // move the char at front to end of gap
    buffer[end] = buffer[gap_start - 1];
    styleBuffer[end] = styleBuffer[gap_start - 1];

    //nulify start position
    buffer[gap_start-1] = null;
    styleBuffer[gap_start-1] = null;

    gap_end += 1;
    gap_start -= 1;
  }
  else
  {

  }
}

 //@current
 //need to take into consideration nulls and stylebuffer values
var insert_char = function insert_char (chr) {
  // alert(gap_start);
  buffer[gap_start] = chr;
  gap_start += 1;
}

var delete_char = function delete_char (){
  buffer[gap_start - 1] = null;
  gap_start--;
}

var insert_multiple_char = function insert_multiple_char (str) {
  str_arr = str.split('');
  for (var i = 0 ; i < str_arr.length; i++)
  {
    insert_char(str_arr[i]);
  }
}

//to be done once selection logic is added
var delete_multiple_char = function delete_multiple_char (start, end){
  //dfs
}


//in memeory will go right up to next physical char i.e
var move_multiple_forward = function move_multiple_forward (num_chr){
  //alert('MMF:: ' + num_chr);
  for(var i = 0; i < num_chr; i++){
    //find physical char
    while(buffer[size - gap_end + 1] == null && gap_end > 1){
      //double nulls ??
      //are inherently handled as they should not be between a -> b
      move_gap_forward();
    }
    //then itt through num_char
    move_gap_forward();//num_chr ++
  }
}

var move_multiple_back = function move_multiple_back (num_chr){
  //alert('MMB:: ' + num_chr);
  for(var i = 0; i < num_chr; i++)
  {
    while(buffer[gap_start - 1] == null && gap_start > 0){
      move_gap_back();
    }
    move_gap_back();
  }
}


// Manage ghost segments to prepare segments for backspace
var manage_ghost_segments = function manage_ghost_segments(){
  var module_or_text = 0; // 1 = text  && 2 = module;
  var search_deletable_content = true;
  while(search_deletable_content){
    if(current_line_segment.prev().index() == -1){
      search_deletable_content = false;
      //end of physical line  == fire backspace-newline methodss
    }else{
        if(current_line_segment.prev().attr('class') != 'line-segment'){
          //you hit a module -- ABORT LOGIC
          //@manipulation-method
          search_deletable_content = false;
          module_or_text = 1;
        }
        else if(current_line_segment.prev().text().length == 0){ //preventative condition for future problems
          current_line_segment.prev().remove();
          //ghost-segment - Delete and continue
        }
        else if(current_line_segment.prev().text().length > 0){
          //make make current_line_segment and current_segment_index = current_line_segment.text().length - 1
          //current_line_segment = current_line_segment.prev();
          //current_segment_index = current_line_segment.prev().text().length -1; //check
          search_deletable_content = false;
        }
    }
  }
  return module_or_text;
}


var back_space_segment_text = function back_space_segment_text(middle_end) {
    //alert() delete backspace teXSt
    if(middle_end == 1){ //middle
      complete_till_prev(); // modify to check for modules?
      delete_char();
      if(current_line_segment.text().length == 1){
        var del_chr = current_line_segment.text();
        if(current_line_segment.prev().index() >= 0){
          current_line_segment = current_line_segment.prev();
          if(current_line_segment.attr('class') == 'module'){
            current_segment_index = 1;
          }else{
            current_segment_index = current_line_segment.text().length;
          }
          current_line_segment.next().remove();
        }else if(current_line_segment.next().index() != -1){
          current_segment_index = 0;
          current_line_segment  = current_line_segment.next();
          current_line_segment.prev().remove();
        }else{
          current_segment_index = 0;
        }
        move_cursor_left(del_chr);
      }else if(current_line_segment.text().length > 1){
        var del_chr = current_line_segment.text().charAt(current_line_segment.text().length -1);
        var str = current_line_segment.text().slice(0, current_line_segment.text().length - 1);
        current_line_segment.text(str);
        current_segment_index -= 1;
        move_cursor_left(del_chr);
      }
    }
    else if(middle_end == 2) {
      complete_till_prev(); // modify to check for modules?
      delete_char();
      if(current_line_segment.prev().text().length == 1){ //delete whole segment;
        var del_chr = current_line_segment.prev().text();
        current_line_segment.prev().remove();
        move_cursor_left(del_chr);
      }
      else if(current_line_segment.prev().text().length > 1){ // next segment is greater than two chars
        //alert("opt 2");
        var del_chr = current_line_segment.prev().text().charAt(current_line_segment.prev().length -1);
        var str = current_line_segment.prev().text().slice(0, current_line_segment.text().length - 1);
        current_line_segment.prev().text(str);
        current_segment_index = current_line_segment.prev().text().length;
        move_cursor_left(del_chr);
        current_line_segment = current_line_segment.prev();
      }else{
        alert("you've hit a ghost");
      }
    }
    else {
    }
}

//back_space
var back_space = function back_space () {
  //testbox ---------------------
  // ----------------------------
  var segment_text = current_line_segment.text().split('');
  //beggining
  var identifier = current_line_segment.attr('class');
  //you are dealing with a module
  if(identifier != 'line-segment'){
     //call module @manipulation-method

  }
  //-------------- YOU ARE DEALING WITH PLAIN TEXT ----------------
  //Beggining of segment
  else if(current_segment_index == 0){
      //Beggining of line
      if(current_line_segment.index() == 0){
        //call move to next-line-segment-cursor. The data has already been depleted
        //just deal with curosr physical movement
        //Begining of para
        if(current_line.index() != 0){//move to new line logic
          var ismodule  = manage_ghost_segments(); // clean up fluff before moving to new line
          if(ismodule == 0){
            //it is a module so you have to delete it and have specific cursor management logic
            //current_line_segment.last();
            //delete previous text /module check for space to move segment ... move segment

          }else{
            delete_module();
            //current_line = current_line.prev();
            //current_line_segment = current_line.
          }
        }else if(current_para.index() > 0){
        //beggining of para not first para
        //1) check for module
        //2) check for fluff[ghost modules]
        //3) merge two paras together
        //4) shift cursor up
        }
      }
      //Beggining of segment
      else{
        var ismodule = manage_ghost_segments();
        if(ismodule == 0){
            back_space_segment_text(2);
        }else{
          //you are deleting a module
          delete_module();
        }
       }
  }
  //you are in the middle||end of a segment
  else if(current_segment_index > 0){
    back_space_segment_text(1);
  }
}


var delete_module = function delete_module() {
  alert("You are deleting a module");
}

/*
var old_back_space = function old_back_space() {
    //check the targeted segment to manipulate has the module class?
    //if not treat it as plain text
   var segment_text = current_line_segment.text().split('');
    //change the backspace functionality to encompass all of the following additional
    //features i wil implement
    if(current_segment_index == 0){
      if(current_line_segment.index() > 0){
        complete_till_prev();
        delete_char();
        //delete whole segment
        if(current_line_segment.prev().text().length == 1){
          current_line_segment = current_line_segment.prev();
          //choose where to assign the segment and index now
          if(current_line_segment.index() > 0 && current_line_segment.prev().text().length > 0){
            //second condition not necessary due to the fact that one can just have segment index at 0
            alert("1");
            var del_chr = current_line_segment.text().charAt(0);
            current_line_segment = current_line_segment.prev();
            current_line_segment.next().remove();
            current_segment_index = current_line_segment.text().length;
            move_cursor_left(del_chr);
          }else if(current_line_segment.index() == 0){
            alert("2");
            var del_chr = current_line_segment.text().charAt(0);
            current_line_segment.text(''); // NB the chr length must be one
            current_segment_index = 0
            move_cursor_left(del_chr);
          }

        }else if(current_line_segment.prev().text().length > 1){
          alert("3");
          current_line_segment = current_line_segment.prev();
          current_segment_index = current_line_segment.text().length - 1;
          var del_chr = current_line_segment.text().charAt(current_line_segment.text().length -1);
          var str = current_line_segment.text().slice(0, current_line_segment.text().length - 1);
          current_line_segment.text(str);
          move_cursor_left(del_chr);
        }
        //current_segment_index = 0;
      }
    }
    //middle : Delete char when cursor is inbetween two chars in middle of segment
    else if(current_segment_index > 0  && current_segment_index < current_line_segment.text().length){
      alert("4");
      complete_till_prev();
      delete_char();
      split_segment_for_space(segment_text);
      var del_chr = current_line_segment.text().charAt(current_line_segment.text().length -1);
      var str = current_line_segment.text().slice(0, current_line_segment.text().length - 1);
      current_line_segment.text(str);
      move_cursor_left(del_chr);
    }
    //end
    else if(current_segment_index > 0  && current_segment_index == current_line_segment.text().length){
      alert("5");
      complete_till_prev(); // moves the data structure back 1 step ... i.e Gap buffer
      delete_char(); //deletes the char in memory
      current_segment_index = current_line_segment.text().length - 1; //changes the current segment to -1
      var del_chr = current_line_segment.text().charAt(current_line_segment.text().length -1);
      // ^^ the char in physical space to delete
      var str = current_line_segment.text().slice(0, current_line_segment.text().length - 1);
      // copys the text of the segment to be all -1 char and then reassigns the text to sefment
      // in the following line
      current_line_segment.text(str);

      //z-debug
      $("#realtime-debugger").text(" 2 " + del_chr);

      //moves the curor left in physical space by the realtime width of the actual del_char
      move_cursor_left(del_chr);
    }
    else{
      alert('ERROR:: back_space');
    }
    //i think the whole back_space method requires a massive overhaul. It tries
    //to hard to deal with excpetions but at a cost of exponential complexity
}
*/
//RULE:: ANY ACTION THAT OCCURS FROM THE EDGE OF A SEGMENT
//       CARRIES THE ATTRIBUTES OF THE ORIGINATING SEGMENT

var add_space = function add_space(){
  var segment_text = current_line_segment.text().split('');

  //At the end
  if(current_segment_index >= current_line_segment.text().length){
    complete_till_prev();
    insert_seg_at(' ');
    insert_space_buffer();
    move_cursor_right(' ');
  }
  //At the begginging
  else if(current_segment_index == 0 && current_line_segment.text().length >= 1){
    //complete_till_prev();
    complete_till_prev();
    insert_seg_at_before(' ');
    insert_space_buffer();
    move_cursor_right(' ');
  }
  else if(current_segment_index > 0  && current_segment_index < current_line_segment.text().length){
    //of spaces....
    //current_line_segment.charCodeAt(current_segment_index +1) == 160
    if(segment_text[current_segment_index] == ' '){
      complete_till_prev();
      split_segment_for_space(segment_text);
      insert_seg_at(' ');// the end of the first tempSeg i.e tempSeg1
      insert_space_buffer();
      move_cursor_right(' ');
    }//of word
    else if(segment_text[current_segment_index] != ' ' || segment_text[current_segment_index] == null){
      //complete_till_prev();
      split_segment_for_space(segment_text);
      insert_seg_at(' ');// the end of the first tempSeg i.e tempSeg1
      insert_space_buffer();
      move_cursor_right(' ');
    }
    else{
      alert("ERROR:: Add_space");
    }
  }
}

var insert_space_buffer = function insert_space_buffer (){
  // body...
  buffer[gap_start] = ' ';
  gap_start += 1;
}
//Split
//transfer
//insert

//after every style change
//space

var cssVals = null;
var update_css_buffer = function update_css_buffer(){
  // body...
  cssVals = previous_line_segment.css([
    "color", "background-color"
  ]);
}

var copy_style = function copy_style(){
  if(cssVals != null && cssVals != undefined){
      current_line_segment.css(cssVals);
  }
}

//operates from current buffer location
//only if a new style is declared in the middle of a segment or style
var insert_styles =  function insert_styles(){
  // body... will iterat through and insert all styles at currently focused position
  if(cssVals != null){
    for(var i = 0; i <  cssVals.length; i++){
      styleBuffer[gap_start] = cssVals[i];
      insert_char(null);
    }
  }
}

// RULE TO NOTEs: ALL STYLEBUFFERS BEFORE A SEGMENT WILL ALWAYS APPLY TO THAT SEGMENT
var split_segment_for_space = function split_segment_for_space(segmentText) {
  // could use the substring method here but it would not be mirroring the underlying data structures
  var tempSeg1 = $("<span class='line-segment'></span>");
  var tempSeg2 = $("<span class='line-segment'></span>");
  //move_multiple_back(current_segment_index);
  //var segmentText = current_line_segment.text().split('');
  for(var i = 0; i < current_segment_index; i++){
    tempSeg1.append(segmentText[i]);
  }
  for(var i = current_segment_index; i < segmentText.length; i++){
    tempSeg2.append(segmentText[i]);
  }
  var segIndex = current_line_segment.index();
  if(segIndex > current_line.children().length){
    current_line.append(tempSeg1);
    current_line.append(tempSeg2);
  }else if(segIndex < 1){
    current_line.prepend(tempSeg2);
    current_line.prepend(tempSeg1);
  }else{
    current_line.children().eq(segIndex).after(tempSeg1);
    tempSeg2.insertAfter(tempSeg1);
  }
  current_line_segment.remove();
  current_line_segment = tempSeg1;
  current_segment_index = tempSeg1.text().length - 1;
}


var split_segment_for_new_word = function split_segment_for_new_word(segmentText) {
  // could use the substring method here but it would not be mirroring the underlying data structures
  var tempSeg1 = $("<span class='line-segment'></span>");
  var tempSeg2 = $("<span class='line-segment'></span>");
  //move_multiple_back(current_segment_index);
  tempSeg1.append(segmentText[0]);
  for(var i = 1; i < segmentText.length; i++){
    tempSeg2.append(segmentText[i]);
  }
  var segIndex = current_line_segment.index();
  if(segIndex > current_line.children().length){
    current_line.append(tempSeg1);
    current_line.append(tempSeg2);
  }else if(segIndex < 1){
    current_line.prepend(tempSeg2);
    current_line.prepend(tempSeg1);
  }else{
    current_line.children().eq(segIndex).after(tempSeg1);
    tempSeg2.insertAfter(tempSeg1);
  }
  current_line_segment.remove();
  current_line_segment = tempSeg2;
  current_segment_index = tempSeg2.length;
}


//Func:: traverse style_buffer and meta data in underlying data structure
//starting points:: current_line_segment
var complete_till_next = function complete_till_next () {
  while(buffer[size - gap_end + 1] == null && gap_end > 1){
    move_gap_forward();
  }
}

//Func:: travers style_buffer and meta data in underlying data structure
//starting point:: current_line_segment
var complete_till_prev = function complete_till_prev (argument) {
  while(buffer[gap_start - 1] == null && gap_start > 0 ){
      move_gap_back();
  }
}

//param l_index:: index of current line
var insert_seg_at = function insert_seg_at (chr) {
  //1 = space
  if(chr == ' '){
    chr = '&nbsp';
  }
  $("<span class='line-segment'>"+chr+"</span>").insertAfter(current_line_segment);
  current_line_segment = current_line_segment.next();
  current_segment_index = 0;
}

var insert_seg_at_before = function insert_seg_at_before(chr) {
  //1 = space
  if(chr == ' '){
    chr = '&nbsp';
  }

  $("<span class='line-segment'>"+chr+"</span>").insertBefore(current_line_segment);
  current_line_segment = current_line_segment.prev();
  current_segment_index = 0;
}

var update_data_cycle = function update_data_cycle() {
  previous_segment_index = current_segment_index;
  previous_line_segment = current_line_segment;
  previous_line = current_line;
  previous_para = current_para;
}

// --------------------End of Data Manipulation -----------------------------------
//-------------------------------------------------------------------------------
//-----------------------------Line Logic --------------------------------------
// [Manager for line logic]
// Background:: Each line is a meta container for line segments that fit within paragraphs
// Function:: Dynamically prepared at runtime/load of document.
// NBd do not confuse with [enter --> new paragraph]
// Calls:: 1) Logic is called at the creation of a new line segment to see if full.
//         2) Logic is called in the addition of a letter to the semgment to see if full.
//
var isLineFull = function isLineFull (singleChar) {
  var line_Width = current_line.width();
  if(singleChar == ' '){
    singleChar = '&nbsp;';
  }
  $("#single-width").html(singleChar);
  var additional_Width = $("#single-width").width();
  line_Width +=  additional_Width;

  if(line_Width > $('#firstPage').width()){
    return true;
  }
  else{
    return false;
  }
}


// ------------------- @Debug -----------------------------------------------------
//on any action update debug so i can see the contents of gap
//buffer array
var debug_itt = 0;


var update_debug_info = function update_debug_info(){

  debug_itt += 1;

  var stringInfo = "";
  stringInfo += "  ***** Debug Info ***** &#13;&#10;";
  stringInfo += "&#13;&#10; Instance# = " + debug_itt;
  stringInfo += "&#13;&#10; Curr Buffer Pos = " + gap_start;
  stringInfo += "&#13;&#10; Curr Buffer End = " + gap_end;
  stringInfo += "&#13;&#10; Size of buffer = " + size;
  stringInfo += "&#13;&#10; Previous segment index" + previous_segment_index;
  stringInfo += "&#13;&#10";

  //calculate buffer insert -- should match cursor
  for (var i = 0; i < buffer.length ; i++){
    if(i == gap_start){
      stringInfo += "[*|*]";
    }

    if(buffer[i] == null){

    }
    else{
      stringInfo += buffer[i];
    }
  };

  $('#buffer-info').html(stringInfo);
}

//TESTING purposes
$('#reset-colors').on('click', function(){
  var all_segments = $('.page').find('.line-segment');
  all_segments.each(function(itt){
    $(this).css({'color':'black'});
    $(this).css({'background-color':'white'});
  });
});



// -------------------------End of Debug -----------------------------------------
//-------------------------------------------------------------------------------

// ---------------- Outstanding
//selection logic
//toolbar for styling
//bullet points
//Module integration
// ------------------------------------------------------------------------------
//-----------------------------INITIALISATION----------------------------------------
var setUpVariables = function setUpVariables () { //manipulation logic initialisation
  previous_line_segment = $(".page div:eq(0) span:eq(0) span:eq(0)"); //0
  previous_line = $(".page div:eq(0)  span:eq(0)"); // 0
  previous_para = $(".page div:eq(0)"); // 0
  previous_segment_index = 0; // 0
  //position the actual gap buffer at the correct area
  //for now hardcoded
  //gap_start = 0;
  //gap_end = 1000 - 72;
  initialise_gap_buffer();

  //keep moving until at beggining of fist para
   while(buffer[size - gap_end + 1] == null && gap_end > 1){
      //double nulls ??
      //are inherently handled as they should not be between a -> b
      move_gap_forward();
  }
}

var initialise_gap_buffer = function initialise_gap_buffer () {
  //function will move back from the end until it reaches the beggining
  //need to determine size of the gap_buffer
  var itt = 0;
  for(var i = (size-1); i >= 0; i--){
    if(buffer[i] == null && styleBuffer[i] == null){
      gap_start = i;
    }
    else{
      break;
    }
  }
  //alert(size - gap_end);
  align_initial_gap_buffer();
}

var align_initial_gap_buffer = function align_initial_gap_buffer () {
  while(gap_start > 0){
    move_gap_back();
  }
  move_gap_back();
}

var destroyUsedData = function destroyUsedData() {
  // destroy objects like segmentpositions that take up too much memory in runtime
}

//=====================================================================================
$(document).on('click', '#reset-debug' , function(e){

});

$(document).on('click', '#show-styleBuffer', function(e){
});

var update_data_debug_2 = function update_data_debug_2 () {
  var data_string = '';
  for(var i = 0; i < size; i++){
    data_string += styleBuffer[i] + '<br/>';
  }
  $('#debug-data').html(data_string);
}


var update_data_debug = function update_data_debug (){
  var data_string = '';
  for(var i = 0; i < size; i++){
    if(buffer[i] == null){
      if(styleBuffer[i] != null){
        data_string += i + '+<br/>';
      }else{
        data_string += i + '*<br/>';
      }
    }
    else{
      data_string += i + ': ' + buffer[i] + '<br/>';
    }
    if(gap_start == i){
      data_string += i + '[*]<br/>';
    }
  }
  $('#debug-data').html(data_string);
}

//----------------------- File Manipulation ---------------------------------------
var asyncOptimisation = function asyncOptimisation (argument) {
  // will go through whole data structure and check that there arnt overlapping styles
  // join multiple spacing etc
}

var asyncSave = function asyncSave (argument) {
  // body...
}
// ====================== End of file Manipulation ===================================
// ---------------------------------------------------------------
// ------------------- End GAP BUFFER ----------------------------




// ------------------- Style Initialistation -----------------------------------------
var load_styles = function load_styles () {
  var allSegments = document.getElementsByClassName("line-segment");
  current_line_segment = null;
  var currSegmentIndex = 0; // stores the current segment index
  var startIndexOfSegment = 0;
  var endIndexOfSegment = 0;
  var styleS = null;
  var prefix = null;
  //how to deal with empty segments ??

  //active style variables
  var current_style_colour = null;
  var color_endPos = null; //[] first val holds a color

  var current_style_size = null;
  var size_endPos = null; //[]

  var current_style_font = null;
  var font_endPos = null; //[]

  var current_style_bold = null;
  var bold_endPos = null; //[]

  var current_style_underline = null;
  var underline_endPos = null; //[]

  var current_style_italic = null;
  var italic_endPos = null; //[]

  //as we go through the styling we encounter new styles we push them in  and need to remember
  // 1) the end of the curr one in the buffer
  // 2) the actual value
  // the inherit orderimplies the order they went into the collection

  var colorMap = [];
  // end of initialisation


  //loop through entire buffer an style
  for(var i = 0; i  < buffer.length; i++){
    // -------------- This logic adds segments to specific line ------
    // -------------- good for bullet point styling etc --------------
    // ----------It also remembers current segment -------------------
    startIndexOfSegment = segmentPositions[currSegmentIndex];

    if(currSegmentIndex < segmentPositions.length){ //you still have enough segments
      endIndexOfSegment = segmentPositions[currSegmentIndex + 1]; // where curr segment ends
      if(i >= endIndexOfSegment){  // if current buffer has exceeded end of segment you are in new current segment
        currSegmentIndex++;
        current_line_segment = allSegments[currSegmentIndex];
      }else{
        current_line_segment = allSegments[currSegmentIndex];
      }
    }
    else{ // we've hit the end of the line
      endIndexOfSegment = buffer.length;
      startIndexOfSegment = segmentPositions[segmentPositions.length];
      current_line_segment = allSegments[allSegments.length];
    }

    //segmentPositions stores start position of all segments
    // ---------------End of logic --------------------------------
    //regex
    // /\|/g
    // /\#(.*?)\|/g;

    if(buffer[i] == null && styleBuffer[i] != null){ //weve found a style key
      var styleS = styleBuffer[i];
      var prefix = styleS.substring(0,2);

      // COLOR STYLE
      if(prefix == '/#'){ // its a colour style
        //color_endPos = parseInt(styleS.split(/\|/g)[1]);
        //current_style_colour = /\#(.*)\|/g.exec(styleS)[1];
        //current_style_colour = '#' + current_style_colour;

        var styleProperties = {
          propertyVal: '#' + (/\#(.*)\|/g.exec(styleS)[1]),
          endPos: parseInt(styleS.split(/\|/g)[1])
        };

        colorMap.unshift(styleProperties);
      }
      //SIZE STYLE
      else if(prefix == '/s'){
        //do font size
      }
      //FONT STYLE
      else if(prefix == '/f'){
        //do font type
      }
    }
    //******
    //*red *
    //*blue*
    //******
    if(colorMap.first() != null){
      if(i > colorMap.first().endPos ){ //by this logic the last number is included in the styling
        colorMap.shift(); //keep popping until we reach endpos that works
        if(colorMap.first() != null && colorMap.first() != undefined){ // its empty
          if(i > colorMap.first().endPos ){ //check if iteration needed [often unlikely]
          var foundViableEndPos = false;
          var endPos = colorMap.first();
            while(!foundViableEndPos){ // now keep shifting until endPos is greater than i
              if(i > colorMap.first().endPos){ //by this logic the last number is included in the styling
                colorMap.shift();
                if(colorMap.first() == null || colorMap.first() == undefined){ // its empty
                  foundViableEndPos = true;
                }else{
                  endPos = colorMap.first();
                }
              }else{
                foundViableEndPos = true;
              }
            }
          }else{
            //do nothing
          }
        }else{
          //do nothing
        }
      }


      //we've found a colorStyle to work with. Now add it to the current segment
      if(colorMap.first() != null && colorMap.first() != undefined){ // its empty
        if(i <= colorMap.first().endPos){
          current_line_segment.style.color = colorMap[0].propertyVal;
        }
      }
    }else{
      //console.log("Do nothing: ColorMap is depleted");
    }
  }
}

if (!Array.prototype.last){
  Array.prototype.first = function(){
      return this[0];
  };
};

//method takes in start pos and end pos of buffer and
//colors in all segments inBetween
var colorInSegments = function colorInSegments(startbufferPosition, endBufferPosition){
  //match position to pos in the relevant segment ????
  // numOfSegments
  // segmentPositions [] ---- stores the start pos of each segment

  //
  var allSegments = document.getElementsByClassName("line-segment");
  var currSegment = 0;
  // 67 -- 22 segments
}

// -------------------- End of style initialising -------------------------------------




// -------------------- Start of Cursor movement ---------------------------------------
// This logic will return a number of how many spaces and chars to move left or right
// when calling the datastructure logic make sure to skip the nulls
// --------------------------- Cursor Movement -------------------------------------
var count_rest_of_segment = function count_rest_of_segment (direction, chr_index) {
  var sum_chars = null;
  //direction = 0 //back /left
  //direction = 1 //forward /right
  if(direction == 1)
  {
    sum_chars += chr_index;
    var prev_size = previous_line_segment.text().length;
    sum_chars += prev_size - previous_segment_index;
  }
  else if(direction == 0)
  {
    //alert('psi: ' + previous_segment_index);
    sum_chars += previous_segment_index;
    var curr_size = current_line_segment.text().length - chr_index;
    sum_chars += curr_size;
  }
  return sum_chars;
}

//Func: counts the untouched segments inbetween the two focuesed segments
//quirk: does not offer wrap around for multi line / multi para
var count_inbetween_segments = function count_inbetween_segments (direction){
  var sum_chr_count = 0;
  var cs = current_line_segment;
  var ps = previous_line_segment; //check exceptions
  var all_segments = null;

  if(direction == 1){
    all_segments = ps.nextUntil(cs, '.line-segment');
  }
  else if(direction ==0){
    all_segments = ps.prevUntil(cs, '.line-segment');
  }

  all_segments.each(function(){
    sum_chr_count += $(this).text().length;
  });

  return sum_chr_count;
}


//This fun replaces the above one to calculate remainder segment length
//if the span is accross lines
var count_outlier_segments = function count_outlier_segments (direction){
  // body...
  var sum_chr_count = 0;
  var prev = previous_line_segment;
  var curr = current_line_segment;

  if(direction == 1){
    var prev_segs = prev.nextAll('.line-segment');
    var curr_segs = curr.prevAll('.line-segment');

    prev_segs.each(function (itt){
      sum_chr_count += $(this).text().length;
    });

    curr_segs.each(function(itt){
      sum_chr_count += $(this).text().length;
    });

  }else if(direction == 0){
    var prev_segs = curr.nextAll('.line-segment');
    var curr_segs = prev.prevAll('.line-segment');


    prev_segs.each(function (itt){
      sum_chr_count += $(this).text().length;
    });

    curr_segs.each(function(itt){
      sum_chr_count += $(this).text().length;
    });

  }

  return sum_chr_count;
}

var count_outlier_lines = function count_outlier_lines (direction) {
  var sum_chr_count = 0;
  if(direction == 1){
    var all_segs = previous_line.nextAll('.line').children('.line-segment');
    var all_segs2 = current_line.prevAll('.line').children('.line-segment');

    all_segs.each(function (itt){
      sum_chr_count += $(this).text().length;

    });
    all_segs2.each(function (itt){
      sum_chr_count += $(this).text().length;
    });
  }

  else if(direction == 0){
    var all_segs = current_line.nextAll('.line').children('.line-segment');
    var all_segs2 = previous_line.prevAll('.line').children('.line-segment');

    all_segs.each(function (itt){
      sum_chr_count += $(this).text().length;
    });

    all_segs2.each(function (itt){
      sum_chr_count += $(this).text().length;
    });
  }

  return sum_chr_count;
}

//EDIT
//counts remaining segments for last line on both sides
var count_remaining_segments = function count_remaining_segments (direction) {
  var sum_chr_count = null;
  if(direction ==1){
    var remaining_segments_curr = current_line.children('.line-segment').first();
    var remaining_segments_prev = previous_line.children('.line-segment').last();
    sum_chr_count += remaining_segments_curr.text().length;
    sum_chr_count += remaining_segments_prev.text().length;
    var all_segments = remaining_segments_curr.nextUntil(current_line_segment);
    var all_segments_prev = remaining_segments_prev.prevUntil(previous_line_segment);

    all_segments.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
    all_segments_prev.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
  }

  if(direction ==0){
    var remaining_segments_curr = current_line.children('.line-segment').last();
    var remaining_segments_prev = previous_line.children('.line-segment').first();
    sum_chr_count += remaining_segments_curr.text().length;
    sum_chr_count += remaining_segments_prev.text().length;

    var all_segments = remaining_segments_curr.prevUntil(current_line_segment);
    var all_segments_prev = remaining_segments_prev.nextUntil(previous_line_segment);

    all_segments.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
    all_segments_prev.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
  }
  return sum_chr_count;
}

var count_inbetween_lines = function count_inbetween_lines (direction){
  var sum_chr_count = 0;
  if(direction == 1){
    var all_segs = previous_line.nextUntil(current_line,'.line').children('.line-segment');
    sum_chr_count = all_segs.length;
  }else if(direction == 0){
    var all_segs = current_line.nextUntil(previous_line, '.line').children('.line-segment');
    sum_chr_count = all_segs.length;
  }
  return sum_chr_count;
}


//same as the method under except it does not iterate through paras to make if faster
var tobefixed = function in_para_segment_count (direction) {
  var sum_chr_count = null;

  //from begging line of para count all lines until selected
  if(direction == 1)
  {
    //count initial child line
    var initial_child_line = current_para.children('.line').first();

    if(!initial_child_line.is(current_line)){
      icl = initial_child_line.find('.line-segment');
      icl.each(function(itt){
            sum_chr_count += $(this).text().length;
          });

      var lines_involved = initial_child_line.nextUntil(current_line);

      lines_involved.each(function( index ){
      var all_line_seg = $(this).find('.line-segment');
        all_line_seg.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
      });
    }

    var initial_child_line_prev = previous_para.children('.line').last()
    if(!initial_child_line_prev.is(previous_line)){
      icl = initial_child_line_prev.find('.line-segment');
      icl.each(function(itt){
            sum_chr_count += $(this).text().length;
          });

      var lines_involved = initial_child_line_prev.prevUntil(previous_line);

      lines_involved.each(function( index ){
      var all_line_seg = $(this).find('.line-segment');
        all_line_seg.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
      });

    }
  }

  //from begging line of para count all lines until selected
  if(direction == 0)
  {
    var last_child_line = current_para.children('.line').last();
    if(!last_child_line.is(current_line)){
      icl = last_child_line.find('.line-segment');
      icl.each(function(itt){
            sum_chr_count += $(this).text().length;
          });

      var lines_involved = last_child_line.nextUntil(current_line);
      lines_involved.each(function( index ){
      var all_line_seg = $(this).find('.line-segment');
      all_line_seg.each(function(itt){
          sum_chr_count += $(this).text().length;
        });
      });
    }


    var initial_child_line_prev = previous_para.children('.line').first()
    if(!initial_child_line_prev.is(previous_line)){
      icl = initial_child_line_prev.find('.line-segment');
      icl.each(function(itt){
            sum_chr_count += $(this).text().length;
          });

      var lines_involved = initial_child_line_prev.nextUntil(previous_line);

      lines_involved.each(function( index ){
      var all_line_seg = $(this).find('.line-segment');
        all_line_seg.each(function(itt){
           sum_chr_count += $(this).text().length;
         });
      });

    }
  }
  return sum_chr_count;
}

//also need logic somewhere in this area to deal with neighbouring paras
//it will be th last call in the count_inbetween_segments chain
var count_inbetween_paras = function count_inbetween_paras (direction) {
  //get all the segmets inbetween and count their chars

  var sum_chr_count = null;
  //we are moving the gap right
  if(direction == 1)
  {
    //calculate in prev_segment_level
    //var paras_involved =
    //Care nextUntil is cyclical
    var paras_involved = previous_para.nextUntil(current_para);

    paras_involved.each(function( index ) {
        var all_line_seg = $(this).find('.line-segment');
        all_line_seg.each(function(itt){
        sum_chr_count += $(this).text().length;
        });
    });
    //alert(sum_chr_count);
  }

  //we are moving the gap left
  if(direction == 0)
  {
    var paras_involved = previous_para.prevUntil(current_para);

    paras_involved.each(function( index ) {
        var all_line_seg = $(this).find('.line-segment');
        all_line_seg.each(function(itt){
        sum_chr_count += $(this).text().length;
        });
    });
  }
  return sum_chr_count;
}

//New method to work with back space
var back_space_move_left = function back_space_move_left(e){


}


//Func: takes in click location e and moves cursor to that position
//Func: All it does is move cursor to physical location
//Return: The index of the char that is after the cursor
//Quirk: will always move segment if the seg_index is last for
var jump_cursor_in_segment = function jump_cursor_in_segment (e) {
  //alert(e.pageX +  "   " + e.pageY);
  // the click logic
  //alert("-"+current_line_segment.text()+"-");
  //posX is the real physical location of the segment starting
  //from the very left point
  //Allows to figure out if event  is to the left or right of the
  //segment in question
  var posX = current_line_segment.offset().left;
  var Xpos = e.pageX - posX;
  //alert("Xpos "+ Xpos + " E.pageX " + e.pageX);
  //$(".check-width").css = current_line_segment.css;
  var count = current_line_segment.text().length;
  var seg_text = current_line_segment.text().split('');
  var prev_char = null;
  var prev_width = null;
  var accumulated_width = null;
  var seg_index = 0;

  var alertTest = '';
  for(var i = 0; i < seg_text.length; i++){
  }


  for(var i = 0; i < count; i++)
  {
    var this_char = null;
    var left_offset_boundary = accumulated_width;

    if(seg_text[i] == ' ')
    {
        this_char = "&nbsp;";
    }
    else
    {
        this_char = seg_text[i];
    }
    //alert(this_char);
    //add to single width
    $("#single-width").html(this_char);
    var curr_width = $("#single-width").width();

    if(prev_char != null)
    {
      if(multi_width != prev_width + curr_width) // multi-width is neccessary for quirks
      {
        $("#multi-width").html(prev_char + this_char);
        var multi_width = $("#multi-width").width();
        accumulated_width += multi_width - prev_width;
        //alert("sdas");
      }
      else
      {
        accumulated_width += curr_width;
      }
    }
    else
    {
      accumulated_width += curr_width;
    }

    if(accumulated_width > Xpos)
    {
        var right_offset = accumulated_width - Xpos;
        var left_offset = Xpos -left_offset_boundary;

        //right half off the chr
        if(right_offset < left_offset || right_offset == left_offset)
        {
          $(".cursor").css({'left': (accumulated_width - 1  + posX) + 'px'});
          var line_height = current_line_segment.height();
          $(".cursor").css({'height': line_height + 'px'});
          var line_top = current_line_segment.offset().top;
          $(".cursor").css({'top': line_top + 'px'});
          seg_index = i + 1;
        }
        //clicked on left half of the chr
        else
        {
          $(".cursor").css({'left': ( left_offset_boundary - 1 + posX) + 'px'});
          var line_height = current_line_segment.height();
          $(".cursor").css({'height': line_height + 'px'});
          var line_top = current_line_segment.offset().top;
          $(".cursor").css({'top': line_top + 'px'});
          //seg_index = i - 1;
          seg_index = i;
        }
      break;
    }
    //Determine correct binding segment
    //There will be three state
    //Beggining of line
    //Middle of line
    //End of line
    prev_char = this_char;
    prev_width = curr_width;
  }
  return seg_index;
}

// ERROR in some instances the back_space stem
var move_cursor_left = function move_cursor_left(chr){
  var cls = current_line_segment.index();
  var special_case_adjust = 0;
  if(chr == ' ')
  {
    chr = "&nbsp;";
  }

  $("#single-width").html(chr);
  var curr_width = $("#single-width").width();
  var e = jQuery.Event( "mousedown", {
  which: 1,
    pageX: $(".cursor").offset().left - curr_width,
    pageY: $(".cursor").offset().top
  });

  jump_cursor_in_segment(e);
  //current_segment_index = jump_cursor_in_segment(e);

  //do i need to update the current line segment ??

  //click_cursor is not necessary because ??
  //click_cursor(current_segment_index);
}

//@current
var move_cursor_right = function move_cursor_right (chr) {
  //alert(current_segment_index);
  //Simple hack:: fake a click event of + width of curr char
  var cls = current_line_segment.index();
  var special_case_adjust = 0;

  if(chr == ' ')
  {
    chr = "&nbsp;";
  }
  //the char in question has been stored in memory
  $("#single-width").html(chr);
  var curr_width = $("#single-width").width();

  if(current_segment_index > 0){
    var length = $("#multi-width").html(current_line_segment.text().charAt(current_segment_index-1) + chr).width();
    var preceeding_char_length = $("#single-width").html(current_line_segment.text().charAt(current_segment_index-1)).width();
    special_case_adjust = (preceeding_char_length + curr_width) - length;
  }
  var e = jQuery.Event( "mousedown", {
  which: 1,
    pageX: $(".cursor").offset().left + curr_width - special_case_adjust,
    pageY: $(".cursor").offset().top
  });
  jump_cursor_in_segment(e);
}

var move_cursor_down = function move_cursor_down () {
}


var move_cursor_up = function move_cursor_up () {
}

//haven't implemented gap start
var move_cursor = function move_cursor (chr_index) {
  //if segment is the same
  //alert(current_line_segment.data() + '  ' + previous_line_segment);
  //alert("gap start: " + gap_start + " gap end" + gap_end);

  if(current_line_segment.is(previous_line_segment))
  {
    //alert("hello");
    if(chr_index > previous_segment_index)
    {
      move_multiple_forward(chr_index - previous_segment_index)
    }
    if(chr_index < previous_segment_index)
    {
      move_multiple_back(previous_segment_index - chr_index);
    }
    if(chr_index == previous_segment_index)
    {
    }
  }
}
//commit index changes


//This method synchronises the data structure with the location
//of the cursor in physical space
var click_cursor = function click_cursor(chr_index)
{
  //in future count lines will not be called
  //as length of each line will be store in the
  //critical counter buffer. [optimisation phase]

  current_line = current_line_segment.parent();
  current_para = current_line.parent();

  var charCounter = null;

  // if still on the same segment
  if(current_line_segment.is(previous_line_segment))
  {
    //where in the segment did i click?
    //chr_index = jump_cursor_in_segment(e);
    //is it greater than previous?
    if(chr_index > previous_segment_index)
    {
      //move gap forward
      charCounter = chr_index - previous_segment_index;
      move_multiple_forward(charCounter);
    }
    //is it less than previous ?
    else if(chr_index < previous_segment_index)
    {
      //move gap back
      charCounter = previous_segment_index - chr_index;
      move_multiple_back(charCounter);
    }
    //do nothing -- exit execution as fast as possible
    else if(chr_index == previous_segment_index)
    {
      //do nothing
    }
  }
  //if still on the same line
  else if (previous_line.is(current_line))
  {
    //move gap right
    if(current_line_segment.index() > previous_line_segment.index())
    {

      charCounter = count_rest_of_segment(1 , chr_index);
      charCounter += count_inbetween_segments(1);
      move_multiple_forward(charCounter);

    }
    //move gap left
    else if(current_line_segment.index() < previous_line_segment.index())
    {
      var charCounter1 = count_rest_of_segment(0 , chr_index);
      var charCounter2 = count_inbetween_segments(0);
      move_multiple_back(charCounter2 + charCounter1);
    }
  }
  // if still on the same paragraph -- count in between lines
  else if(previous_para.is(current_para))
  {
    if(current_line.index() > previous_line.index())
    {
      charCounter = count_rest_of_segment(1 , chr_index);
      charCounter += count_outlier_segments(1);
      charCounter += count_inbetween_lines(1);
      move_multiple_forward(charCounter);
    }
    else if(current_line.index() < previous_line.index())
    {
      charCounter = count_rest_of_segment(0 , chr_index);
      charCounter += count_outlier_segments(0);
      charCounter += count_inbetween_lines(0);
      move_multiple_back(charCounter);
    }
  }

  //working with different paragrphs
  else if(!previous_para.is(current_para))
  {
    //chr_index = jump_cursor_in_segment(e);
    //alert("in different para");
    //gap will move right
    if(current_para.index() > previous_para.index())
    {
      charCounter = count_rest_of_segment(1 , chr_index);
      charCounter += count_outlier_segments(1);
      charCounter += count_outlier_lines(1);
      charCounter += count_inbetween_paras(1);
      move_multiple_forward(charCounter);
    }
    else if (current_para.index() < previous_para.index() ){
      charCounter = count_rest_of_segment(0 , chr_index);
      charCounter += count_outlier_segments(0);
      charCounter += count_outlier_lines(0);
      charCounter += count_inbetween_paras(0);
      move_multiple_back(charCounter);
    }
    //calculate sum of segments in in between paras
    //calculate sum of lines in between segments
    //calculate segment offsets
  }
  //configure vars for next event cycle
  //previous_segment_index = chr_index;
  //this backwards movement is not sticking
  previous_segment_index = chr_index;
  previous_line_segment = current_line_segment;
  previous_line = current_line;
  previous_para = current_para;
}


// THE BIG CHECKS ______________ YES
var check_out_of_bounds = function check_out_of_bounds () {
  var boundwidth = current_line.width();

  var child_combined_width = null;
  current_line.children('span').each(function () {
      var curr_item = $(this);
      child_combined_width += curr_item.width();
  });
  if(child_combined_width > boundwidth - 10)
  {
   return true;
  }
  else
  {
    return false;
  }
}

/*
$(document).on('click', '.module', function(e){
    alert("fired");
});

$(".module").click(function(e){
  alert("fired");
  var mod_left_mov = $(this).offset().left + $(this).outerWidth();
  $('.cursor').css({"left": mod_left_mov+ "px"});
});
*/
// ----------------------------End of Cursor movement ---------------------------
//-------------------------------------------------------------------------------
// --------------------- End of Cursor Movement ----------------------------------------



// ---------------------------- Start of display logic ---------------------------------


//Load Initial document
var load_document = function load_document () {

  //ajax call to populate buffer
  //load_buffer i.e  @load_buffer

  //This is how we do
  for(var i = 0; i < buffer.length; i++){
    //waitForFirstItem ==  to see if we are in new segment and to update the queue called segmentPositions
    //waitingForNextWord ==  if a segment creating char is clicked -- if this val is true will not create new word
    //                    --> basically to see if previous was charachter or space
    // segStyleInit == flag to allow for multiple null values to be stacked next to eachother

    var val = buffer[i];
    new_line_logic(val);

    //depending on type of value in buffer[i] append to editor
    if(val == null){
      //do stylebuffer Logic
      styleIncomming(i);
    }
    else if(val == ' '){
      if(waitingForNextWord == false){
         new_line_segment();
         current_line_segment.append("&nbsp;");
         waitingForNextWord = true;
         segmentPositions.push(i);
      }
      else{
        current_line_segment.append("&nbsp;");
        if(waitForFirstItem == true){
        segmentPositions.push(i);
        waitForFirstItem = false; // this flag is for new para logic
        }
      }
      segStyleInit = true;
    }
    else{
      if(waitingForNextWord == true){     // ( current_line_segment == null || waitingForNextWord == true){}
        new_line_segment();
        segmentPositions.push(i);
        waitingForNextWord = false;
      }
      current_line_segment.append(buffer[i]);
      if(waitForFirstItem == true){
        segmentPositions.push(i);
        waitForFirstItem = false;
      }
      segStyleInit = true;
    }

  }
  //this is bad need a workaround for looping every time
  //some way to switch from segment to segment
  //nothing comes for free
  //splitEndOfStyle();
  //alert("allSegments: " + allSegments.length);
  //alert("segmentPositions: " + segmentPositions.length);
}


// what if null inbetween space and space or letter and letter
var splitEndOfStyle = function splitEndOfStyle(){
    var segPos = null;
    var segStart = null;
    var bufferRemaining = 0;
    var bufferMidIndex  = null;

    //create the array of jQuery objects
    var allSegments = [];
    $(".line-segment").each(function() {
      allSegments.push($(this));
    });


    for(var i = 0; i < endPosOfStyleQueue.length; i++){ // each is a split provided it meets the requirements
      //start if else chain
      var pos = endPosOfStyleQueue[i]; // the start pos of the segment
      //var allSegments = document.getElementsByClassName("line-segment");
      if(buffer[pos] == buffer[pos + 1] && buffer[pos] == ' '){ // if space or space
        //do the spliting operating
        var index = getMatchingSegment(pos); // now we have the segment we want to split segmentPositions[index]

        current_line_segment = allSegments[index]; // until method is over this is the line segment we will be working with
        current_line = current_line_segment.parent();
        segStart = segmentPositions[index];
        bufferMidIndex = pos - segStart;

        if(index == allSegments.length - 1){
          var continueLoop = true;
          var currentIndex = 0;
          while(continueLoop){ // to deal with edge case
            currentIndex = segStart + bufferMidIndex + bufferRemaining;
            if(styleBuffer[currentIndex] == null && buffer[currentIndex] == null){
              continueLoop = false;
            }else{
              bufferRemaining += 1;
            }
          }
        }else{
          bufferRemaining = segmentPositions[index + 1] - segmentPositions[index] - bufferMidIndex; //what if edge case ??
        }
        //Now to do the actual splitting 1 for loop for each half
        if(bufferMidIndex > 0){
          splitSegment(bufferMidIndex, segStart, pos, bufferRemaining);
          //because you split a two spaces
          //remember the load_document function has finished running
          endPosOfStyleQueue.shift();
          //segmentPositions.push(i);
        }
      }else if(buffer[pos] != null && buffer[pos + 1] != null && buffer[pos] != ' ' && buffer[pos + 1] != null){
        // so not null and neither is space: therefore letter and letter
        var pos = endPosOfStyleQueue[i];
        var index = getMatchingSegment(pos); // now we have the segment we want to split segmentPositions[index]
        current_line_segment = allSegments[index]; // until method is over this is the line segment we will be working with
        current_line = current_line_segment.parent();
        segStart = segmentPositions[index]; //start position of each
        bufferMidIndex = pos - segStart;

        if(index == allSegments.length - 1){
          var continueLoop = true;
          var currentIndex = 0;
          while(continueLoop){ // to deal with edge case
            currentIndex = segStart + bufferMidIndex + bufferRemaining;
            if(styleBuffer[currentIndex] == null && buffer[currentIndex] == null){
              continueLoop = false;
            }else{
              bufferRemaining += 1;
            }
          }
        }else{
          bufferRemaining = segmentPositions[index + 1] - segmentPositions[index] - bufferMidIndex; //what if edge case ??
        }
        if(bufferMidIndex > 0){
          splitSegment(bufferMidIndex, segStart, pos, bufferRemaining);
          //because you split a word
          endPosOfStyleQueue.shift(); //does it matter what order these are done in ?
          //segmentPositions.push(i);
        }
      }else{
        //do nothing
      }
    }
}


var splitSegment = function splitSegment (bufferMidIndex, segStart, pos, bufferRemaining) {
// could use the substring method here but it would not be mirroring the underlying data structures
  var tempSeg1 = $("<span class='line-segment'></span>");
  var tempSeg2 = $("<span class='line-segment'></span>");

  //move_multiple_back(current_segment_index);

  //var segmentText = current_line_segment.text().split('');
  for(var i = 0; i < bufferMidIndex; i++){
    tempSeg1.append(buffer[segStart + i]);
  }
  for(var i = 0; i < bufferRemaining; i++){
    tempSeg2.append(buffer[pos + i]);
  }

  var segIndex = current_line_segment.index();

  if(segIndex > current_line.children().length){
    current_line.append(tempSeg1);
    current_line.append(tempSeg2);
  }else if(segIndex < 1){
    current_line.prepend(tempSeg2);
    current_line.prepend(tempSeg1);
  }else{
    current_line.children().eq(segIndex).after(tempSeg1);
    tempSeg2.insertAfter(tempSeg1);
  }

  current_line_segment.remove();
  current_line_segment = tempSeg1;
  current_segment_index = tempSeg1.length;
  //@test
}

var splitSegments = function splitSegment (bufferMidIndex, segStart, pos, bufferRemaining) {
  //buffer and styleBuffer do not need to be modified as these are endings
  //1) remove the segment from dom and replace with two new segments
  //2) update segmentPositions array which stores the beggining of each segment

  // could use the substring method here but it would not be mirroring the underlying data structures
  var tempSeg1 = $("<span class='line-segment'></span>");
  var tempSeg2 = $("<span class='line-segment'></span>");

  for(var x = 0; x < bufferMidIndex; x++){
    tempSeg1.append(buffer[segStart + x]);
  }
  for(var x = 0; x < bufferRemaining; x++){
    tempSeg2.append(buffer[pos + x]);
  }
  //stage 1
  var segIndex = current_line_segment.index();

  current_line_segment.remove();
  if(segIndex > current_line.children().length){
    current_line.append(tempSeg1);
    current_line.append(tempSeg2);
  }else if(segIndex < 1){
    current_line.prepend(tempSeg2);
    current_line.prepend(tempSeg1);
  }else{
    current_line.children().eq(segIndex -1).after(tempSeg1);
    current_line.children().eq(segIndex).after(tempSeg2);
  }
  //tempSeg2.css({'color':'red'});

  //stage 2
  var segPosIndex = segmentPositions.indexOf(segStart); // the position of segment in segment positions NB!:: does not account for empty segments
  segmentPositions.slice(segPosIndex,1, tempSeg2);
  segmentPositions.slice(segPosIndex,0, tempSeg1);
}

//This method takes in i which is the start position of the segment that you want to find
// and returns itt which is the position of the segment in segmentPositions[] and allSegments[]
var getMatchingSegment = function getMatchingSegment (i){
  for(var itt = 0; itt < segmentPositions.length; itt++){
    if(itt != segmentPositions.length -1){ // you are not at the end

      if(segmentPositions[itt] == segmentPositions[itt+1]){
        if(i == segmentPositions[itt]){
          return itt;
        }
      }
      else if(i >= segmentPositions[itt] && i < segmentPositions[itt+1]){
          return itt;
      }
    }else{
       return itt; // you may be wrong but at least you returned something
    }
  }
}


var new_line_logic = function new_line_logic (val) {

    if(current_para != null && current_line != null && current_line_segment != null && val != null){
      if(isLineFull(val)){
        if(val == " "){ //end of previous word
          //create new line
          new_line();
          waitingForNextWord = false;
          //new_line_segment();
        }else if(waitingForNextWord == true){ // currently wating for new word
          new_line();
          //new_line_segment();
        }else if(current_line_segment.text().length > 0){ //in the middle of current segment
            if(current_line_segment.text().charAt(0) != ' '){
              current_line_segment.remove();
              new_line();
              current_line.append(current_line_segment);
            }
        }else if(val == null){
          //do nothing
        }
      }
    }
 }


var styleIncomming = function styleIncomming (index) {
  var val = styleBuffer[index];
  var styleS = null;
  var prefix = null;
  try{
    if(styleBuffer[index] != null){
      styleS = styleBuffer[index];
      prefix = styleS.substring(0,2);
    }
  }catch(err){
    console.log("regex is less than two chars length");
  }


  switch(prefix){
    case "pa":
        new_paragraph();
        if(initialised == true){
          if(segStyleInit == true){ // a new line segment needs to be made
            new_line();
            new_line_segment();
          }else{ // theres a line segment to take that was stacked before this para
            current_line_segment.remove();
            new_line();
            current_line.append(current_line_segment);
          }
        }else{
          new_line();
          new_line_segment();
          initialised = true;
          segmentPositions.push(index);
        }
        waitForFirstItem = true; // this flag waits until a letter or space is added to the segment to update it
        segStyleInit = false;

        break;
    case "/#":
          if(segStyleInit == true){ //if first in line of null styles
            new_line_segment();
            waitingForNextWord = false;
            segStyleInit = false;
          }
          endPosOfStyleQueue.push(parseInt(styleS.split(/\|/g)[1]));
          //endPosOfStyleQueue.push(/\#(.*)\|/g.exec(styleS)[2]);
          colorQueue.push(styleBuffer[index]);
        break;
    case "/s":
        if(segStyleInit == true){ //if first in line of null styles
          new_line_segment();
          waitingForNextWord = false;
          segStyleInit = false;
        }
        endPosOfStyleQueue.push(parseInt(styleS.split(/\|/g)[1]));
        //endPosOfStyleQueue.push(/\#(.*)\|/g.exec(styleS)[2]);
        sizeQueue.push(styleBuffer[index]);
        break;
    case "/f":
        if(segStyleInit == true){ //if first in line of null styles
          new_line_segment();
          waitingForNextWord = false;
          segStyleInit = false;
        }
        endPosOfStyleQueue.push(parseInt(styleS.split(/\|/g)[1]));
        //endPosOfStyleQueue.push(/\#(.*)\|/g.exec(styleS)[2]);
        fontQueue.push(styleBuffer[index]);
        break;
    default:
        console.log("Check styleIncomming");
  }
}

//adds new line segment after current line segment
var new_line_segment = function new_line_segment(){
   if(current_line == null){
    new_line();
   }
   var new_line_segment = $("<span class='line-segment'></span>");
   current_line.append(new_line_segment);
   previous_line_segment = current_line_segment;
   current_line_segment =  new_line_segment;
   numOfSegments += 1;
}

//adds new line after current line
var new_line = function new_line () {
  console.log("Um what ?!");
  if(current_para == null){
    new_paragraph();
  }
  var line  =  $("<span class='line'></span>");
  current_para.append(line);
  previous_line = current_line;
  current_line = line;
}

//adds new paragraph after current paragraph
var new_paragraph = function new_paragraph (){
  var new_para = $("<div class='para'></div>");
  if(current_para == null){
    $('#firstPage').append(new_para);
  }
  else{
    new_para.insertAfter(current_para);
  }
  previous_para = current_para;
  current_para = new_para;

  //previous_line = current_line;
  //previous_line_segment = current_line_segment;
  //current_line = null;
  //current_line_segment = null;
}
// --------------------------- End of display logic  ----------------------------------
//------------------------------------------------------------------------------------
