from django.db import models
from mongoengine import *
from mongoengine import connect
connect('supply_proj_db')

#reference ::: Equivalent to mysql joins
#          ----> Use for non vital data concatenations
#embedding ::: Concatenates the data all the data can be accessed in one operation
#          ----> Important for data sets that have strong associations

"""
These models are the basis for all users on site
No matter the nature of the user they must have a user accounts
The id of the user must be synchronised with the id of user from
the mysql database which is likely to handle transactions
"""
# class Supplier(Document):
#      name = StringField()
#
# class Servicer(EmbeddedDocument):
#     name = StringField()
#
# class User(Document):
#     name = StringField()
#     supplyAllias = ListField(EmbeddedDocumentField(Supplier))
#     serviceAllias = ListField(EmbeddedDocumentField(Servicer))
#
#
# #Outline :: Indiviual supply line for a specific user
# #:: All supply lines are different and customisable within suppliers params
# # class SupplyLine(EmbeddedDocument):
# #     name = StringField()
# #     consumer = ReferenceField(User)
# #     supplier = ReferenceField(User)
# #     productInfo = StringField()
# #
# #
# # # :: See SupplyLine notes
# # class ServiceLine(EmbeddedDocument):
# #     name = StringField()
# #     consumer = ReferenceField(Consumer)
# #     servicer = ReferenceField(Servicer)
# #     serviceInfo = StringField()
# #
# #
# # class SupplyGroups(Document):
# #     name = StringField()
# #     consumer = ReferenceField(Consumer)
# #     supplyLines = ListField(EmbeddedDocumentField(SupplyLine))
# #     serviceLines = ListField(EmbeddedDocumentField(ServiceLine))
# #
# #
# #
# #
# #
# # # is the completed service or product after payment and successful negotiations
# # # For both service and products
# # class FinalisedLine(Document):
# #     name = StringField()
# #     consumer = ReferenceField(User)
#     producer = ReferenceField(User)


#for any embedded links such as goto supplier.
#def get_absolute_url(self):
    #from django.urls import reverse
    #return reverse('people.views.details', args=[str(self.id)])
