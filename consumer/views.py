from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core import serializers
from django.urls import reverse
from consumer.models import *

# Create your views here.
@login_required
def dashboard(request):
    return render(request, 'consumer/dashboard.html', {'section': 'dashboard'})

@login_required
def proto1(request):
    return render(request, 'consumer/proto1.html', {'section': 'proto1'})

"""
@login_required
def editor(request):
    return render(request, 'consumer/editor.html', {'section':'editor'})
"""

@login_required
def gettestdata(request):
    return HttpResponse("test")

@login_required
def ctest(request):
    return HttpResponse("Ctest")

@login_required
def loadgroups1(request):
    try:
        m=[]
        m.append("<div id='supplygroupscontainer'>")
        m.append("<div class='group'  id='group1'>1</div>")
        m.append("<div class='group' id='group2'>2</div>")
        m.append("<div class='group' id='group3'>3</div>")
        m.append("<div class='group' id='group4'>4</div>")
        m.append("</div>")
        mn = ''.join(m)
        return HttpResponse(mn)
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)


@login_required
def loadgroups(request):
    try:
        m=[]
        m.append("<div id='supplygroupscontainer'>")
        m.append("<div class='group'  id='group1'>1</div>")
        m.append("<div class='group' id='group2'>2</div>")
        m.append("<a href=''>Hey ho</a>")
        m.append("<div class='group' id='group3'>3</div>")
        m.append("<div class='group' id='group4'>4</div>")
        m.append("</div>")
        mn = ''.join(m)
        return HttpResponse(mn)
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)

@login_required
def urltestfire(request):
    HttpResponse("this was a succesful link fire")

@login_required
def loaddata(request):
    try:
        michael = Consumer(name="Michael Johnstone").save()
        return HttpResponse("success")
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)

@login_required
def getdata(request):
    try:
        data = User.objects.first()
        return HttpResponse(data.name)
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)
