from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^$', views.dashboard, name="dashboard"),
    #url(r'^editor/$', views.editor, name="editor"),
    url(r'^loadgroups/$', views.loadgroups, name="groups"),
    url(r'^urltestfire/$', views.urltestfire, name="urltestfire"),
    url(r'^loaddata/$', views.loaddata, name="loaddata"),
    url(r'^getdata/$', views.getdata, name="getdata"),
    url(r'^gettestdata/$', views.gettestdata, name="gettestdata"),
    url(r'^ctest/$', views.ctest, name="ctest"),
    url(r'^proto1/$', views.proto1, name="proto1"),
]
