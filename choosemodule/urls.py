from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^choosem/$', views.choosem, name="choosem"),
    url(r'^modselec/$', views.modselec, name="modselec"),
    url(r'^router/$', views.router, name="router"),
]
