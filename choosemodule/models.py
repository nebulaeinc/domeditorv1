from django.db import models
from mongoengine import *
from mongoengine import connect
connect('supply_proj_db')

class Moduleopt(Document):
    modfamily = StringField()
    modpic = StringField()
    modtitle = StringField()

class Twittersel(Document):
    modfamily = StringField()
    modtag = StringField()
    modpic = StringField()
    modtitle = StringField()

class ModuleRouter(Document):
    modfamily = StringField()
    modtag = StringField()

class Modules(Document):
    name = StringField()
    html = StringField()
    js = StringField()
    css = StringField()

class Toolbox(Document):
     name = StringField()
     script = StringField()
     html = StringField()
     html2 = StringField()
     css = StringField()
