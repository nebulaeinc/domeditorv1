from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core import serializers
from django.urls import reverse
from twitterapi.models import *
import json
import urllib.request
from bson import json_util

def createTimeline(request):
    try:
        username = request.GET['name']
        p = Test(username=username, function='createTimeline', script='<script type = "text/javascript" async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>', html='<div id="twitterCreateTimeline"> <a class="twitter-timeline" href="https://twitter.com/'+  username  +'">Tweets by'+ username +'</a></div>')
        p.save()
        x = Test.objects.filter(username=username)[0]
        div = " <div> "+  x.script + x.html   +" </div> "
        return HttpResponse(div)
        #return JsonResponse(n)
        #return JsonResponse(serializers.serialize('json', x), safe=False)
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)



def PostTweet(request):
    try:
        username = request.GET['name']
        p = Posttweet(username=username, function='PostTweet', script='<script type = "text/javascript" async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>', html='<div id="twitterPostTweet"> <a href="https://twitter.com/share" class="twitter-share-button" data-show-count="false">Tweet</a></div>').save()
        x = Posttweet.objects.filter(username=username)[0]
        div = " <div> "+  x.script + x.html   +" </div> "
        return HttpResponse(div)
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)


def DirectMessage(request):
    try:
        username = request.GET['name']
        p = Directmessage(username=username, function='Directmessage', script='<script type = "text/javascript" async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>', html='<div id="twitterDirectMessage"> <a href="https://twitter.com/messages/compose?recipient_id="'+ username + ' class="twitter-dm-button" data-screen-name="https://twitter.com/"' + username + 'data-show-count="false">Message @https://twitter.com/' + username +'</a></div>').save()
        x = Directmessage.objects.filter(username=username)[0]
        div = " <div> "+  x.script + x.html   +" </div> "
        return HttpResponse(div)
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)

def Createsingletweet(request):
    try:
        postid = request.GET['postid']
        #raw = urllib.urlopen("https://api.twitter.com/1.1/statuses/oembed.json?id="+postID).read()
        with urllib.request.urlopen("https://api.twitter.com/1.1/statuses/oembed.json?id="+postid) as url: raw = url.readall().decode('utf-8')
        changed = json.loads(raw)
        html_raw = changed['html']
        html, script = str(html_raw).split('<script')
        p = Createst(postid=postid, function='createSingleTweet', script='<script type = "text/javascript" async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>', html='<div id="twitterCreateSingleTweet"> '+ html +'</div').save()
        x = Createst.objects.filter(postid=postid)[0]
        div = " <div> "+  x.script + x.html   +" </div> "
        return HttpResponse(div)
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)


def router(request):
    try:
        response = HttpResponse("Here's the text of the Web page.")
        selectedmod = request.GET['method']
        #raw = urllib.urlopen("https://api.twitter.com/1.1/statuses/oembed.json?id="+postID).read()
        if selectedmod == "Createsingletweet":
           response = Createsingletweet(request)
        elif selectedmod == "DirectMessage":
           response = DirectMessage(request)
        elif selectedmod == "PostTweet":
           response = PostTweet(request)
        elif selectedmod == "createTimeline":
          response = createTimeline(request)
        else:
           response = HttpResponse("Here's the text of the Web page.")
        return response
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)


@login_required
def gettestdata(request):
    return HttpResponse("this api works")




    """
    p = createTimeline(username='MrMittens948', function='createTimeline', script='<script type = "text/javascript" async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>', html='<div id="twitterCreateTimeline"> <a class="twitter-timeline" href="https://twitter.com/MrMittens948">Tweets by MrMittens948</a></div>' )
    p.save()
    """
    """
    timeline = createTimeline.create(
        username= username ,
        function= 'createTimeline',
        script  = '<script type = "text/javascript" async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>',
        html    = '<div id="twitterCreateTimeline"> <a class="twitter-timeline" href="https://twitter.com/' + userName + '">Tweets by ' + userName + '</a></div>')
    """
