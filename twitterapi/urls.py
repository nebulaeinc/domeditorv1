from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^createTimeline/$', views.createTimeline, name="createTimeline"),
    url(r'^PostTweet/$', views.PostTweet, name="PostTweet"),
    url(r'^gettestdata/$', views.gettestdata, name="gettestdata"),
    url(r'^DirectMessage/$', views.DirectMessage, name="DirectMessage"),
    url(r'^Createsingletweet/$', views.Createsingletweet, name="Createsingletweet"),
    url(r'^router/$', views.router, name="router"),
]
