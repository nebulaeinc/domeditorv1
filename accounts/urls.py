from django.conf.urls import include, url
from django.contrib import admin
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
	url(r'^login/$', auth_views.login, name="login"),
	url(r'^logout/$', auth_views.logout, name="logout"),
	url(r'^logout-then-login/$', auth_views.logout_then_login, name='logout_then_login'),
]
