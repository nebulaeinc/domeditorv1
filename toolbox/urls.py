from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^$', views.toolbox, name="toolbox"),
    url(r'^getjs/$', views.getjs, name="getjs"),
    url(r'^toolboxPack/$', views.toolboxPack, name="toolboxPack"),
    #url(r'^editor/$', views.editor, name="editor"),
]
