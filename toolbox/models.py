from django.db import models
from mongoengine import *
from mongoengine import connect
connect('supply_proj_db')

class Toolbox(Document):
     name = StringField()
     script = StringField()
     html = StringField()
     htmlside = StringField()
     css = StringField()
