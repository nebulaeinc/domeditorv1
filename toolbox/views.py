from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core import serializers
from django.urls import reverse
from toolbox.models import *

# Create your views here.
@login_required
def toolbox(request):
    try:
        name = request.GET['name']
        x = Toolbox.objects.get(name=name)
        div = "<div class='toolbox-outer-container'>"+ x.html +" </div>"
        return HttpResponse(div)
        #return JsonResponse(n)
        #return JsonResponse(serializers.serialize('json', x), safe=False)
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)

def getjs(request):
    try:
        name = request.GET['name']
        x = Toolbox.objects.get(name=name)
        return HttpResponse(x.script)
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)


def sidepannel(request):
    try:
        name = request.GET['name']
        x = Toolbox.objects.get(name=name)
        return HttpResponse(x.script)
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)


def toolboxPack(request):
    try:
        name = request.GET['name']
        moduletoolbox = Toolbox.objects.get(name=name)
        tools = {}
        tools['script'] = moduletoolbox.script
        tools['html'] = moduletoolbox.html
        tools['htmlside'] = moduletoolbox.htmlside
        tools['css'] = moduletoolbox.css
        return JsonResponse(tools)
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse(r)
