from django.apps import AppConfig


class NavibarConfig(AppConfig):
    name = 'navibar'
