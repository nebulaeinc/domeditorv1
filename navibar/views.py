from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core import serializers

# Create your views here.
@login_required
def navibar(request):
    return render(request, 'navibar/base.html')
    #return HttpResponse('hello')
